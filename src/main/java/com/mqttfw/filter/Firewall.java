package com.mqttfw.filter;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TimerTask;

import com.mqttfw.model.Environment;
import com.mqttfw.model.MQTTPacketInputStream;
import com.mqttfw.model.MQTTPacketOutputStream;
import com.mqttfw.model.Rule;
import com.mqttfw.model.RuleDelay;
import com.mqttfw.model.RulePeriodic;

public class Firewall {
	Environment env = null;
	List<Rule> rules = new ArrayList<Rule>();
	MQTTPacketInputStream in = null;
	MQTTPacketOutputStream out = null;
	
	Object lock;
	
	public Firewall() {
		this.env = new Environment(); //out);
		
		// rationale: write on the waiting-queue, read from the filtered-queue
		this.out = new MQTTPacketOutputStream(env.getPacketQueueWaiting());
		this.in = new MQTTPacketInputStream(env.getPacketQueueFiltered());

	}
	
	public Environment getEnvironment() {
		return this.env;
	}
	
	public InputStream getInputStream() {
		return this.in;
	}
	
	public OutputStream getOutputStream() {
		return this.out;
	}
	
	public void addRule(Rule r, String name) {
		r.setName(name);
		this.rules.add(r);
	}

	public void addRule(Rule r) {
		String ruleName = "r" + this.rules.size();
		this.addRule(r, ruleName);
	}

	public void addRule(RulePeriodic r, String name) {
		TimerTask tt = new TimerTask() {

			@Override
			public void run() {
				try {
					if (r.isEnabled(env)) {
						// try {
							// synchronized (env) {
								// env.wait();
								env = r.apply(env);
								// env.notifyAll();
							// }
						// } catch (InterruptedException e) {
						// 	// TODO Auto-generated catch block
						// 	e.printStackTrace();
						// }
						
					}
				} catch (Exception e) {
					System.err.println("Error applying the following rule: " + r.toString() + ". Error: " + e.getMessage());
					e.printStackTrace();
				}
			}
			
		};
		this.env.getTimer().scheduleAtFixedRate(tt, 0, r.getPeriod());
	}

	public void addRule(RuleDelay r, String name) {
		TimerTask tt = new TimerTask() {

			@Override
			public void run() {
				if (r.isEnabled(env)) {
					// try {
					// 	synchronized (env) {
					// 		env.wait();					
							env = r.apply(env);
					// 		env.notifyAll();
					// 	}
					// } catch (InterruptedException e) {
					// 	// TODO Auto-generated catch block
					// 	e.printStackTrace();
					// }
				}
			}
			
		};
		this.env.getTimer().schedule(tt, r.getDelay());
	}
	
	public List<Rule> getRules() {
		return this.rules;
	}
	
	public void write(byte[] buffer) throws IOException {
		this.write(buffer, 0, buffer.length);
	}

	public synchronized void write(byte[] buffer, int offset, int nBytes) throws IOException {
		
		// write in the waiting-queue
		
		//Arrays.copy
		//this.getOutputStream().write(buffer, offset, nBytes);
		if (this.env.isVerbose()) {
			System.out.println("Writing MQTT packet (" + nBytes + " bytes) to the waiting queue ...");
		}

		this.getOutputStream().write(Arrays.copyOfRange(buffer, offset, nBytes)); // TODO this is temporary hack, becuse MQTTPacketOutputStream.write(int arg0) is not implemented yet 
			
		if (this.env.getWaitingQueue().isEmpty()) {
			System.out.println("This is odd: MQTT packet was written but it does not appear in the queue... ");
			return;
		}
		
		//Boolean atLeastOne = false;
		for (Rule r : this.rules ) {
			if (r.isEnabled(this.env)) {
				// the rule decides whether something has to be 
				// written in the filtered-queue
				//atLeastOne = true;
				if (this.env.isVerbose()) {
					System.out.println("Rule " + r.getName() + " does apply ...");
				    System.out.println("Queues sizes *BEFORE* : WAITING QUEUE -> " + this.env.getWaitingQueue().size() + " ; FILTERED QUEUE -> " + this.env.getFilteredQueue().size());
				}

				// try {
					// synchronized (this.env) {
					// 	this.env.wait();

						this.env = r.apply(this.env);
						// if (this.env.isVerbose()) {
						//     System.out.println("Queues sizes *AFTER* : WAITING QUEUE -> " + this.env.getWaitingQueue().size() + " ; FILTERED QUEUE -> " + this.env.getFilteredQueue().size());
						// }
					// 	this.env.notifyAll();
					// };
					if (r.isFinal()) {
						break; 
					}
				// } catch (InterruptedException e) {
				// 	// TODO Auto-generated catch block
				// 	e.printStackTrace();
				// }
			} else {
				if (this.env.isVerbose()) {
					System.out.println("Rule " + r.getName() + " does not apply");
				}
			}
		}
//		if(!atLeastOne){
			this.env.getWaitingQueue().clear();
//		}
		if (this.env.isVerbose()) {
			System.out.println("Queues sizes *AFTER* : WAITING QUEUE -> " + this.env.getWaitingQueue().size() + " ; FILTERED QUEUE -> " + this.env.getFilteredQueue().size());
		}

	}	

	public int read(byte[] buffer) throws IOException {
		return this.getInputStream().read(buffer);
	}
}
