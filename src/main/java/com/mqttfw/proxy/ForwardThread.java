package com.mqttfw.proxy; /**
 * ForwardThread handles the TCP forwarding between a socket input stream (source)
 * and a socket output stream (destination). It reads the input stream and forwards
 * everything to the output stream. If some of the streams fails, the forwarding
 * is stopped and the parent thread is notified to close all its connections.
 */
 
import java.io.InputStream;
import java.io.OutputStream;
import com.mqttfw.filter.Firewall;

public class ForwardThread extends Thread
{
    private static final int READ_BUFFER_SIZE = 8192;
 
    InputStream mInputStream = null;
    OutputStream mOutputStream = null;
    
    ForwardServerClientThread mParent = null;
    Firewall mFirewall = null;

    Exception failure = null;
 
    /**
     * Creates a new traffic forward thread specifying its input stream,
     * output stream and parent thread
     */
    public ForwardThread(ForwardServerClientThread aParent, InputStream aInputStream, OutputStream aOutputStream)
    {
        mInputStream = aInputStream;
        mOutputStream = aOutputStream;
        mParent = aParent;
    }
    
    public ForwardThread(ForwardServerClientThread aParent, InputStream aInputStream, OutputStream aOutputStream, Firewall aFirewall)
    {
        this(aParent, aInputStream, aOutputStream);
        this.mFirewall = aFirewall;
    }


    /**
     * Runs the thread. Until it is possible, reads the input stream and puts read
     * data in the output stream. If reading can not be done (due to exception or
     * when the stream is at his end) or writing is failed, exits the thread.
     */
    public void run()
    {
        byte[] buffer = new byte[READ_BUFFER_SIZE];
        try {
			while (true) {
                // System.out.println("Thread (" + this.getName() + ") wait to read ...");
				int bytesRead = mInputStream.read(buffer);
                // System.out.println("Thread (" + this.getName() + ") read " + bytesRead + " bytes ...");
				
				if (bytesRead == -1)
				   break; // End of stream is reached --> exit the thread
				
                String sBuffer = "" + buffer[0];
                String sBufferHex = String.format("%02X", buffer[0]);
                for (int i=1; i<bytesRead; i++) {
                    sBuffer = sBuffer + " " + buffer[i];
                    sBufferHex = sBufferHex + " " + String.format("%02X", buffer[i]);
                    
                }
                // System.out.println("Buffer read ((DEC:" + sBuffer + ")) ((HEX:" + sBufferHex + "))");

				if (this.mFirewall != null) {
                    mFirewall.write(buffer, 0, bytesRead);
					int bytesReadAfter = mFirewall.read(buffer);
                    //assert (bytesRead == bytesReadAfter); // TODO this is temporary
                    // System.out.println("Bytes read after: " + bytesReadAfter);
                    bytesRead = bytesReadAfter;
				}
				
                if (bytesRead > 0) {
                    // System.out.println("Thread (" + this.getName() + ") waits to write " + bytesRead + " bytes ...");
				    mOutputStream.write(buffer, 0, bytesRead);
                    // System.out.println("Thread (" + this.getName() + ") wrote " + bytesRead + " bytes ...");

                }
			}
        } catch (Exception e) {
        	System.err.println("Thread (" + this.getName() + "): Error while reading/writing; connection is broken thus the thread is killed");
        	e.printStackTrace(); //(System.err);
            // Read/write failed --> connection is broken --> exit the thread
            //this.failure = e;
        }

        // Notify parent thread that the connection is broken and forwarding should stop
        System.out.println("Thread (" + this.getName() + ") breaks connection ...");

        //System.out.println("SKIP connectionBroken method");
		mParent.connectionBroken(); // TODO uncomment this - FS
    }

    public Exception getFailure() {
        return this.failure;
    }
}
