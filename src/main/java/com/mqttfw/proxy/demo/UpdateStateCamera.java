package com.mqttfw.proxy.demo;

import java.util.Collection;

import org.json.JSONObject;

import com.mqttfw.model.Environment;
import com.mqttfw.model.MQTTPacket;
import com.mqttfw.model.expressions.Expression;
import com.mqttfw.model.expressions.TimedObject;
import com.mqttfw.model.expressions.Variable;
import com.mqttfw.model.expressions.TimedObject.TimedObjectImplementation;
import com.mqttfw.model.statements.Statement;
import com.mqttfw.proxy.demo.FallModel.FallEffect;
import com.mqttfw.proxy.demo.FallModel.IMAGE_STATES;

public class UpdateStateCamera implements Statement {

    String sensorName;
    Variable stateVar;
    Variable payload;

    public UpdateStateCamera(String sensorName, String stateVarName, String payload) {
        this.sensorName = sensorName;
        this.stateVar = new Variable(stateVarName);
        this.payload = new Variable(payload);
    }

    @Override
    public  Environment apply(Environment env) {
        /*
        Collection<TimedObjectImplementation> items = (Collection<TimedObjectImplementation>) this.buffer.eval(env);

        IMAGE_STATES currState = IMAGE_STATES.NORMAL; //(String) this.state.eval(env);
        
        synchronized (items) {
            for (TimedObjectImplementation to : items) {
                try {
                    MQTTPacket curr = (MQTTPacket)to.getValue(); 
                    // String payload = new String(curr.getPayload());
                    JSONObject jobj = curr.getJSON();

                    if (! jobj.getString("id").equals(this.sensorName)) {
                        continue;
                    }

                    Boolean value = jobj.getBoolean("value");
                    FallEffect<IMAGE_STATES> res = new FallModel().getImageState(currState, value);
                    currState = res.state;
                } catch (RuntimeException e) {
                    System.err.println("Error while updating state camera: " + e.getMessage());
                    e.printStackTrace();
                    continue;
                }
            }
        }
        */
        IMAGE_STATES currState = IMAGE_STATES.valueOf((String)this.stateVar.eval(env));

        JSONObject payload = (JSONObject)this.payload.eval(env);
        String sensorName = payload.getString("id");
        if (sensorName.equals(this.sensorName))
        {
            Boolean value = payload.getBoolean("value");

            FallEffect<IMAGE_STATES> eff = new FallModel().getImageState(currState, value);
            
            System.out.println("UPDATE : " + this.stateVar.getName() + " : " + currState.name() + " -> " + eff.state.name());
            env.setValue(this.stateVar.getName(), eff.state.name());
            // NB no need to check for reset of clock variables
        }
        return env;
    }
    

    public String toString() {
        return "UpdateStateCamera(" + this.sensorName + "," + this.stateVar + "," + this.payload + ")";
    }
}