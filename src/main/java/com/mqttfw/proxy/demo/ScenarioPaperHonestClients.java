package com.mqttfw.proxy.demo;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.time.Clock;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

import org.fusesource.mqtt.codec.PUBLISH;

import com.mqttfw.filter.Firewall;
import com.mqttfw.model.Environment;
import com.mqttfw.model.Rule;
import com.mqttfw.model.RulePeriodic;
import com.mqttfw.model.expressions.And;
import com.mqttfw.model.expressions.Constant;
import com.mqttfw.model.expressions.Equal;
import com.mqttfw.model.expressions.GT;
import com.mqttfw.model.expressions.GTE;
import com.mqttfw.model.expressions.JSONField;
import com.mqttfw.model.expressions.JSONParse;
import com.mqttfw.model.expressions.LT;
import com.mqttfw.model.expressions.Len;
import com.mqttfw.model.expressions.Negate;
import com.mqttfw.model.expressions.NotEqual;
import com.mqttfw.model.expressions.Or;
import com.mqttfw.model.expressions.PacketHasType;
import com.mqttfw.model.expressions.ReadPacket;
import com.mqttfw.model.expressions.ReadPayload;
import com.mqttfw.model.expressions.Tautology;
import com.mqttfw.model.expressions.TimedObject;
import com.mqttfw.model.expressions.TimedObjectTimer;
import com.mqttfw.model.expressions.TimedObjectValue;
import com.mqttfw.model.expressions.Variable;
import com.mqttfw.model.statements.Alert;
import com.mqttfw.model.statements.Append;
import com.mqttfw.model.statements.Assignment;
import com.mqttfw.model.statements.CopyCat;
import com.mqttfw.model.statements.ForEach;
import com.mqttfw.model.statements.IfThenElse;
import com.mqttfw.model.statements.Log;
import com.mqttfw.model.statements.Remove;
import com.mqttfw.model.statements.Sequence;
import com.mqttfw.model.statements.Alert.LEVEL;
import com.mqttfw.proxy.demo.FallModel.ACC_STATES;
import com.mqttfw.proxy.demo.FallModel.FallEffect;
import com.mqttfw.proxy.demo.FallModel.IMAGE_STATES;

import org.json.JSONObject;

public class ScenarioPaperHonestClients extends Scenario {


    // enum ACC_STATES {
    //     NORMAL,
    //     FREE_FALL,
    //     IMPACT,
    //     FELT        
    // };

    // enum IMAGE_STATES {
    //     NORMAL,
    //     FELT
    // };

    class Patient {

        int numFalls = 1;
        int period = 10000;
        double heartBeatMean = 70;
        double heartBeatStdDev = 1;
        long startClock = -1; 
        long[] fallsClocks = null;

        // long deltaFalling = 1500; // was: 1000;

        long deltaFallingBefore = -500;
        long deltaFallingAfter = 3000;
        
        public Patient() {
            // startClock = readClock();
            this(1, 10000, 70, 1);
        }

        private long readClock() {
            return Clock.systemDefaultZone().millis();
        }



        public Patient(int numFalls, int period, double heartBeatMean, double heartBeatStdDev) {
            this.numFalls = numFalls;
            this.period = period;
            this.heartBeatMean = heartBeatMean;
            this.heartBeatStdDev = heartBeatStdDev;

            if (period < 0 || period > 120000) {
                throw new RuntimeException("The period must be at least 1.000 ms and at most 120.000 ms");
            }

            this.fallsClocks = new long[numFalls];
            for (int i=0; i<numFalls; i++) {
                long nextFall = ThreadLocalRandom.current().nextLong(500, period - 500);
                this.fallsClocks[i] = nextFall;
                System.out.println("Patient falls : " + Integer.toString(i) + " : " + Long.toString(nextFall));
            }
            Arrays.sort(this.fallsClocks);

            startClock = readClock();
        }

        public boolean isFalling() {
            int fallingId = fallIdentifier();
            if (fallingId >= this.numFalls) {
                throw new RuntimeException("Unexpected fallingId: " + fallingId + ". Allowed values in the interval [-1," + Integer.toString(this.numFalls-1) + "]");
            }
            long sinceStart = this.readClock() - this.startClock;
            System.out.println("fallingId : " + Long.toString(sinceStart) + " : " + fallingId);
            return fallingId >= 0; 
        }

        private int fallIdentifier() { 

            for (int i=0; i<this.numFalls; i++) {
                long sinceStart = readClock() - startClock;
                // long delta = Math.abs(sinceStart - this.fallsClocks[i]);
                long delta = sinceStart - this.fallsClocks[i];
                if (this.deltaFallingBefore < delta && delta < this.deltaFallingAfter) {
                    return i;
                }
            }

            return -1;
        }

        public long getHeartBeat() {
            double someValue = ThreadLocalRandom.current().nextGaussian(); 
            if (isFalling()) {
                // during a fall, the bell is "shrinked" around the double of the regular heart beats
                return (long)(someValue * (this.heartBeatStdDev/2) + (this.heartBeatMean*2));
            } else {
                // when not falling, the bell falls the given gaussian parameters (mean and standard deviation)
                return (long)(someValue * this.heartBeatStdDev + this.heartBeatMean);
            }
        }

        // private double line(double x, double xA, double yA, double xB, double yB) {
        //     double d = (xA - xB);
        //     return x * ((yA - yB)/d) + (xA * yB - yA * xB)/d; 
        // }

        public double getAcceleration() {
            //double someGaussianValue = ThreadLocalRandom.current().nextGaussian();
            
            int fallId = this.fallIdentifier();
            double value;

            if (fallId >= 0) {
                // when fall identified by fallId is happening, the acceleration depends on the 
                // time-distance between now and the time of the fall
                long now = readClock();
                long delta = now - startClock - this.fallsClocks[fallId];
                // if (delta < -200 || delta >= 500) {
                //     // far away from the fall
                //     value = ThreadLocalRandom.current().nextDouble(-0.01, 0.01); 
                // } else if (delta >= -200 && delta < -100) {
                //     // F1 : acceleration = -200 - 199.4/100 * delta
                //     value = line(delta, -200, 0, -100, -0.7);
                // } else if (delta >= -100 && delta < 0) {
                //     // F2
                //     value = line(delta, -100, -0.7, 0, 0);
                // } else if (delta >= 0 && delta < 100) {
                //     // F3
                //     value = line(delta, 0, 0, 100, 2.3);
                // } else if (delta >= 100 && delta < 200){
                //     // F4
                //     value = line(delta, 100, 2.3, 600, -0.4);
                // } else if (delta >= 200 && delta < 500) {
                //     // F5
                //     value = line(delta, 600, -0.4, 900, 0);
                // } else {
                //     throw new RuntimeException("Piecewise function not well defined for value: " + Long.toString(delta));
                // }
                // System.out.println("Acceleration : " + Long.toString(delta)  + " Now : " + Long.toString(now) + " Start : " + Long.toString(startClock) + " Value : " + Double.toString(1 + value));

                value = FallModel.getAcceleration(delta);

            } else {
                // when not falling, the acceleration is more or less 1G
                value = 1 + ThreadLocalRandom.current().nextDouble(-0.01, 0.01); 
            }

            return value; 
        }


    }

    abstract class Sensor<T> {
        protected Patient p;
        protected String name;
        protected int numSamples;
        int delayMin;
        int delayMax;

        public Sensor(Patient patient, String name, int numSamples, int delayMin, int delayMax) {
            this.name = name;
            this.p = patient; //new Patient();
            this.numSamples = numSamples;
            this.delayMin = delayMin;
            this.delayMax = delayMax;
        }

        public abstract T getValue();

        public void start() {
            // try {

                // a generic sensor is a mqtt client publishing messages
                StandaloneClient sensor = new StandaloneClient(this.name);
            
                Runnable r = new Runnable() {
    
                    @Override
                    public void run() {
                        try {
                            System.out.println("Sensor " + name + " connecting ...");

                            sensor.connect();

                            for (int i=0; i < numSamples; i++) {
                                Object sensorValue = getValue();
                                System.out.println("Sensor : " + name + " : Value : " + sensorValue);
                                sensor.publish("mqttfirewall", "{ id: '" + name + "', value: '" + String.valueOf(sensorValue) + "' }");
                
                                // generate a random delay and then sleep
                                int someDelay = ThreadLocalRandom.current().nextInt(delayMin, delayMax);
                                try {
                                    Thread.sleep(someDelay);
                                } catch (InterruptedException e) {
                                    System.err.println("Error sleeping in sensor " + name + ": " + e.getMessage());
                                    e.printStackTrace();
                                }    
                            }
                            
                            Thread.sleep(2000);
                            System.out.println("Sensor " + name + " ended its task");
                        } catch (Exception e) {
                            System.err.println("Error running sensor thread: " + e.getMessage());
                            e.printStackTrace();
                        } finally {
                            System.out.println("Sensor disconnecting from MQTT broker ...");
                            sensor.disconnect();

                        }
                    }
                    
                };
    
                Thread t = new Thread(r);
                t.start();
            // } catch (Exception err) {
            //     System.err.println("Error launching heart rate sensor: "  + err.getMessage());
            //     err.printStackTrace(System.err);
    
            // }
    
        }
    }

    class HeartBeatSensor extends Sensor<Long> {

        public HeartBeatSensor(Patient patient, String name, int numSamples, int delayMin, int delayMax) {
            super(patient, name, numSamples, delayMin, delayMax);
        }

        @Override
        public Long getValue() {
            double imprecision = ThreadLocalRandom.current().nextDouble(0.95,1.05);
            return (long)(this.p.getHeartBeat() * imprecision);
        }
        
    }

    class AccelerationSensor extends Sensor<Double> {

        public AccelerationSensor(Patient patient, String name, int numSamples, int delayMin, int delayMax) {
            super(patient, name, numSamples, delayMin, delayMax);
        }

        @Override
        public Double getValue() {
            double imprecision = ThreadLocalRandom.current().nextDouble(0.99,1.01);
            double value = this.p.getAcceleration() * imprecision;
            return value;
        }
    }

    class ImageSensor extends Sensor<Boolean> {

        public ImageSensor(Patient patient, String name, int numSamples, int delayMin, int delayMax) {
            super(patient, name, numSamples, delayMin, delayMax);
        }

        @Override
        public Boolean getValue() {
            return this.p.isFalling();
        }
        
    }

    int timeWindow = 2000;
    int taskPeriodicity = 100;
    int fallRatio = 90;
    int securityThreshold = 2;
    int subscriberMaxWaitSeconds = 5;
    int doctorMaxAttempts = 10;

    @Override
    public void setFirewallRules(Firewall f) {

        // just one simple rule: accept everything and pass it through
        Rule r0 = new Rule(new Negate(new PacketHasType(PUBLISH.TYPE)), new CopyCat(), true);
        Rule r1 = new Rule(new Tautology(), new Sequence(
                new Assignment("last", new ReadPacket()),
                new Assignment("jsonLast", new JSONParse(new ReadPayload("last"))), // TODO <- check why this returns a null object
                new Assignment("lastSensor", new JSONField("jsonLast", "id")),
                new Assignment("lastValue", new JSONField("jsonLast", "value")),
                new IfThenElse(new Or(
                        new Equal("lastSensor", new Constant("camera")),
                        new Equal("lastSensor", new Constant("acceleration_smartphone")),
                        new Equal("lastSensor", new Constant("acceleration_smartwatch"))),
                    // new Append("buffer", new TimedObject("last")),
                    // new CopyCat(null, "last")) 
                    new Sequence(
                        new UpdateStateAcceleration("acceleration_smartphone", "state_smartphone", "timer_smartphone", "jsonLast"),
                        new UpdateStateAcceleration("acceleration_smartwatch", "state_smartwatch", "timer_smartwatch", "jsonLast"),
                        new UpdateStateCamera("camera", "state_camera", "jsonLast"),
                        new CopyCat(null, "last")),
                    new CopyCat(null, "last")
                )
            ));
        /*
        Rule r2 = new Rule(new GTE(new Len("buffer"), new Constant(1)), 
            new Sequence(
                new UpdateStateAcceleration("acceleration_smartwatch", "sw_state", "buffer"),
                new UpdateStateAcceleration("acceleration_smartphone", "sp_state", "buffer"),
                new UpdateStateCamera("camera", "camera_state", "buffer"),
                new Assignment("score", new ComputeScore("camera_state", "sw_state", "sp_state")),
                new IfThenElse(new And(new LT("score", new Constant(0.5)), new Equal("sw_state", new Constant(ACC_STATES.FELT.name()))),
                    new Alert(LEVEL.WARN, "smartwatch tampered")
                ),
                new IfThenElse(new And(new LT("score", new Constant(0.5)), new Equal("sp_state", new Constant(ACC_STATES.FELT.name()))),
                    new Alert(LEVEL.WARN, "smartphone tampered")
                ),
                new IfThenElse(new Equal("score", new Constant(0.5)),
                  new Alert(LEVEL.WARN, "/Ue")
                ),
                new IfThenElse(new And(new GT("score", new Constant(0.5)), new NotEqual("sp_state", new Constant(ACC_STATES.FELT.name()))),
                  new Alert(LEVEL.WARN, "smartphone tampered")
                ),
                new IfThenElse(new And(new GT("score", new Constant(0.5)), new NotEqual("sw_state", new Constant(ACC_STATES.FELT.name()))),
                  new Alert(LEVEL.WARN, "smartwatch tampered")
                ) ,
                // new Log(new Len(new Variable("buffer")), "LOG : Buffer len before: "),
                new ForEach("buffer", "curr",
                    new Sequence( 
                        // new Log(new TimedObjectTimer("curr"), "LOG : Timer value: "),
                        new IfThenElse(new GT(new TimedObjectTimer("curr"), new Constant(2500l)), 
                                        new Sequence(
                                            new CopyCat(null, new TimedObjectValue("curr")),
                                            new Remove("curr")
                                        )
                        )
                    )
                )
                // new Log(new Len(new Variable("buffer")), "LOG : Buffer len after: ")
            )); 
        */

        f.addRule(r0, "r0");
        f.addRule(r1, "r1");
        // f.addRule(r2, "r2");
    }

    @Override
    public void setupEnvironment(Environment env) {
        //env.setVerbose();

        env.addVariable("last");
        env.addVariable("buffer", new ArrayList<Object>());
        env.addVariable("jsonLast");
        env.addVariable("lastSensor");
        env.addVariable("lastValue");

        env.addVariable("state_camera");
        env.addVariable("state_smartphone");
        env.addVariable("state_smartwatch");
        env.addClockVariable("timer_smartphone");
        env.addClockVariable("timer_smartwatch");
    }

    @Override
    public void startPublishers() {

        Patient patient = new Patient();
        // Sensor<?> s1 = new HeartBeatSensor(patient, "heart_beat", 50, 800, 1200);
        Sensor<?> s2 = new AccelerationSensor(patient, "acceleration_smartwatch", 500, 100, 120);
        Sensor<?> s3 = new AccelerationSensor(patient, "acceleration_smartphone", 500, 100, 120);
        Sensor<?> s4 = new ImageSensor(patient, "camera", 500, 100, 120);

        // s1.start(); // TODO restore this after the other sensors are working
        s2.start();
        s3.start();
        s4.start();
    }

    @Override
    public void startSubscribers() {
        StandaloneClient the_doctor = new StandaloneClient("doctor");
        System.out.println("Doctor connecting ...");
        try {
            the_doctor.connect();

            the_doctor.subscribe("mqttfirewall");
            
            Runnable r = new Runnable() {

                ACC_STATES sp_state = ACC_STATES.NORMAL;
                ACC_STATES sw_state = ACC_STATES.NORMAL;
                IMAGE_STATES camera_state = IMAGE_STATES.NORMAL;

                long sp_clock_t;
                long sw_clock_t;

                void transitionSmartphone(Double acceleration) {
                    if (this.sp_clock_t == 0) {
                        throw new RuntimeException("You did not initialize your local timer");
                    }
                    long delta = Clock.systemDefaultZone().millis() - sp_clock_t;
                    

                    FallEffect<ACC_STATES> res = new FallModel().getAccelerationState(sp_state, delta, acceleration);

                    System.out.println("SP : " + sp_state.name() + " -(" + acceleration + " @ " + delta + ")-> " + res.state.name());
                    sp_state = res.state;
                    if (res.resetClock) {
                        sp_clock_t = Clock.systemDefaultZone().millis();
                    }

                }

                void transitionSmartwatch(Double acceleration) {
                    if (this.sw_clock_t == 0) {
                        throw new RuntimeException("You did not initialize your local timer");
                    }
                    long delta = Clock.systemDefaultZone().millis() - sw_clock_t;
                    
                    FallEffect<ACC_STATES> res = new FallModel().getAccelerationState(sw_state, delta, acceleration);


                    System.out.println("SW : " + sw_state.name() + " -(" + acceleration + " @ " + delta +")-> " + res.state);
                    sw_state = res.state;
                    if (res.resetClock) {
                        sw_clock_t = Clock.systemDefaultZone().millis();
                    }


                }

                void transitionCamera(Boolean value) {
                    
                    FallEffect<IMAGE_STATES> res = new FallModel().getImageState(camera_state, value);

                    camera_state = res.state;

                }

                Double getScore() {
                    Double score = 0.0;
                    if (camera_state == IMAGE_STATES.FELT) {
                        score += 0.5;
                    }
                    if (sw_state == ACC_STATES.FELT) {
                        score += 0.25;
                    }
                    if (sp_state == ACC_STATES.FELT) {
                        score += 0.25;
                    }

                    System.out.println("Doctor: (CAM:" + camera_state.name() + ",SW:" + sw_state.name() + ",SP:" + sp_state.name() + ") => Score: " + score);

                    return score;
                }

                @Override
                public void run() {

                    this.sw_clock_t = this.sp_clock_t = sw_clock_t = Clock.systemDefaultZone().millis();

                    int n_attempts = doctorMaxAttempts;
                    while (true) {
                        String payload = the_doctor.receiveMessage(subscriberMaxWaitSeconds);
                        
                        if (payload == null) {
                            n_attempts--;
                            if (n_attempts < 0) {
                                System.out.println("Doctor does not see any more messages to received. Stop here.");                                
                                the_doctor.disconnect();
                                break;
                            } else {
                                try {
                                    Thread.sleep(2000);
                                } catch (InterruptedException e) {
                                    System.err.println("Error while waiting for MQTT packets: " + e.getMessage());
                                    e.printStackTrace();
                                    break;
                                }
                                continue;
                            }
                        }

                        n_attempts = doctorMaxAttempts;

                        JSONObject jobj = new JSONObject(payload);
                        // System.out.println("Subscribed : Sensor: " + jobj.get("id") + " Value : " + jobj.get("value"));

                        String sensor_id = jobj.get("id").toString();
                        String value = jobj.get("value").toString();
                        System.out.println("Payload: " + payload);
                        switch (sensor_id) {
                            case "acceleration_smartphone":
                                transitionSmartphone(Double.parseDouble(value));
                                break;

                            case "acceleration_smartwatch":
                                transitionSmartwatch(Double.parseDouble(value));
                                break;

                            case "camera":
                                transitionCamera(Boolean.parseBoolean(value));
                                break;

                            default:
                                System.out.println("Sensor ignored: id:'" + sensor_id + "' value:" + value);
                        }

                        Double score = getScore();
                        if (score > 0.5) {
                            System.out.println("Doctor : Score: " + score + ". Fall detected.");
                        } else {
                            System.out.println("Doctor : Score: " + score + ". No fall");
                        }
                    }
                    
                }
                
            };

            Thread t = new Thread(r);
            t.start();
        } catch (Exception e) {
            System.err.println("Error connecting the_doctor : " + e.getMessage());
            e.printStackTrace();
        }
    }

    protected static PrintStream outputFile(String name) throws FileNotFoundException {
        return new PrintStream(new BufferedOutputStream(new FileOutputStream(name)), true);
    }

    public static void main(String[] args) {

        System.out.println("Working Directory = " + System.getProperty("user.dir"));

        Scenario s = new ScenarioPaperHonestClients();

        try {
            System.setOut(outputFile("scenario_honests.log"));
            System.setErr(outputFile("scenario_honests.err"));

            s.run(args);
        } catch (FileNotFoundException e) {
            System.err.println("Unable to set output and error files: " + e.getMessage());
            e.printStackTrace();
        }
    }
    
}
