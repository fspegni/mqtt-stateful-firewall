package com.mqttfw.proxy.demo;

import com.mqttfw.filter.Firewall;
import com.mqttfw.model.Environment;
import com.mqttfw.model.Rule;
import com.mqttfw.model.expressions.Tautology;
import com.mqttfw.model.statements.CopyCat;


public class ScenarioPassThrough extends Scenario {

    @Override
    public void setFirewallRules(Firewall f) {
        
        // just one simple rule: accept everything and pass it through
        Rule r0 = new Rule(new Tautology(), new CopyCat());
        f.addRule(r0, "r0");
    }

    @Override
    public void setupEnvironment(Environment env) {
        env.setVerbose();
    }

    public static void main(String[] args) {
        Scenario s = new ScenarioPassThrough();

        s.run(args);
    }
    
}
