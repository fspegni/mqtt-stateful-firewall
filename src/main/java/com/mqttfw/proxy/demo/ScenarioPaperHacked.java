package com.mqttfw.proxy.demo;

import java.time.Clock;
import java.util.concurrent.ThreadLocalRandom;

public class ScenarioPaperHacked extends ScenarioPaperHonestClients {
    class HackedHeartBeatSensorAlwaysHigh extends HeartBeatSensor {

        public HackedHeartBeatSensorAlwaysHigh(Patient patient, String name, int numSamples, int delayMin, int delayMax) {
            super(patient, name, numSamples, delayMin, delayMax);
        }

        @Override
        public Long getValue() {
            // return twice the heart-beat value
            return super.getValue() * 2;
        }
    }

    class HackedHeartBeatSensorAlwaysLow extends HeartBeatSensor {

        public HackedHeartBeatSensorAlwaysLow(Patient patient, String name, int numSamples, int delayMin, int delayMax) {
            super(patient, name, numSamples, delayMin, delayMax);
        }

        @Override
        public Long getValue() {
            // return half the heart-beat value
            return super.getValue() / 2;
        }
    }

    class HackedAccelerationSensorAlwaysHigh extends AccelerationSensor {
        
        public HackedAccelerationSensorAlwaysHigh(Patient patient, String name, int numSamples, int delayMin, int delayMax) {
            super(patient, name, numSamples, delayMin, delayMax);
        }

        @Override
        public Double getValue() {
            return ThreadLocalRandom.current().nextDouble(1.9, 2.1); 
        }
    }


    class HackedAccelerationSensorAlwaysLow extends AccelerationSensor {
        
        public HackedAccelerationSensorAlwaysLow(Patient patient, String name, int numSamples, int delayMin, int delayMax) {
            super(patient, name, numSamples, delayMin, delayMax);
        }

        @Override
        public Double getValue() {
            return ThreadLocalRandom.current().nextDouble(0.5, 0.7); 
        }
    }

    class HackedAccelerationSensorAlwaysFall extends AccelerationSensor {
        
        long start;

        public HackedAccelerationSensorAlwaysFall(Patient patient, String name, int numSamples, int delayMin, int delayMax) {
            super(patient, name, numSamples, delayMin, delayMax);
            this.start = readClock();
        }

        @Override
        public Double getValue() {
            // TODO riprendere qui

            long now = readClock();
            long delta = now - this.start;
            // return ThreadLocalRandom.current().nextDouble(0.5, 0.7); 
            return FallModel.getAcceleration(delta - 200); // -200 is an offset used by the FallModel
        }

        private long readClock() {
            return Clock.systemDefaultZone().millis();
        }

    }

    class HackedImageSensorAlwaysHigh extends ImageSensor {

        public HackedImageSensorAlwaysHigh(Patient patient, String name, int numSamples, int delayMin, int delayMax) {
            super(patient, name, numSamples, delayMin, delayMax);
        }

        public Boolean getValue() {
            return true;
        }
    }


    class HackedImageSensorAlwaysLow extends ImageSensor {

        public HackedImageSensorAlwaysLow(Patient patient, String name, int numSamples, int delayMin, int delayMax) {
            super(patient, name, numSamples, delayMin, delayMax);
        }

        public Boolean getValue() {
            return true;
        }
    }

    @Override
    public void startPublishers() {
        // TODO Customize this, in order to start hacked sensors instead of
        // honest sensors
        super.startPublishers();
    }
}
