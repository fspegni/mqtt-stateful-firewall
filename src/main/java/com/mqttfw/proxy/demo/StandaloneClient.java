package com.mqttfw.proxy.demo;

import org.fusesource.mqtt.client.BlockingConnection;
import org.fusesource.mqtt.client.MQTT;
import org.fusesource.mqtt.client.Message;
import org.fusesource.mqtt.client.QoS;
import org.fusesource.mqtt.client.Topic;

import java.io.*;
import java.util.concurrent.TimeUnit;

public class StandaloneClient {

    int port = 1883;
    DataInputStream in;
    DataOutputStream out;
    BufferedReader tastiera;
    BlockingConnection connection;

    String clientId = "the_client";
    String hostname = "localhost";

    public StandaloneClient() {
        
    }

    public StandaloneClient(String clientId) {
        this.clientId = clientId;
    }

    public StandaloneClient(String clientId, String hostname, int port) {
        this.clientId = clientId;
        this.hostname = hostname;
        this.port = port;
    }

    public void connect() throws Exception {
        try {
            MQTT mqtt = new MQTT();
            mqtt.setClientId(clientId);
            // mqtt.setHost(InetAddress.getLocalHost().getHostAddress(), port);
            mqtt.setHost(this.hostname, this.port);
            connection = mqtt.blockingConnection();
            connection.connect();
            System.out.println("Connection done ...");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void publish(String topic, String text){
        try{

            // System.out.println("Client writes (" + topic + ") : " + text);
            connection.publish(topic, text.getBytes(), QoS.AT_MOST_ONCE, false);

        } catch (Exception e) {
            System.err.println("Error publishing on topic '" + topic + "': " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void subscribe(String topic) {
        Topic[] ta = new Topic[1];
        ta[0] = new Topic(topic, QoS.AT_LEAST_ONCE);

        try {
            connection.subscribe(ta); 
        } catch (Exception err) {
            System.err.println("Error subscribing topic '" + topic + "': " + err.getMessage());
            err.printStackTrace();
        }
    }

    public void receiveMessages() {
        while (true) {
            Message m;
            try {
                m = connection.receive();
                // System.out.println("Received message on topic: " + m.getTopic());
                String payload = new String(m.getPayload(), "UTF-8");
                System.out.println("Received message on topic: " + m.getTopic() + " : " + payload);
                
                m.ack();
            
            } catch (Exception e) {
                System.err.println("Error receiving message: " + e.getMessage());
                e.printStackTrace();
            }
            
        }
        

    }

    public String receiveMessage(int numSeconds) {
        Message m;
        String payload = null;
        try {
            m = connection.receive(numSeconds, TimeUnit.SECONDS);
            if (m == null) {
                return null;
            }
            // System.out.println("Received message on topic: " + m.getTopic());
            payload = new String(m.getPayload(), "UTF-8");
            // System.out.println("Received message on topic: " + m.getTopic() + " : " + payload);
            
            m.ack();
        
        } catch (Exception e) {
            System.err.println("Error receiving message: " + e.getMessage());
            e.printStackTrace();
        }

        return payload;

    }

    public void disconnect() {
        if (! this.connection.isConnected()) {
            return;
        }

        try {
            this.connection.disconnect();
        } catch (Exception e) {
            System.err.println("Error while disconnecting the client: " + e.getMessage());
            e.printStackTrace();
        }
    }


    public static void main(String[] aArgs) throws Exception {
        StandaloneClient c = new StandaloneClient("the_publisher");
        c.connect();
        c.publish("mqttfirewall", "foo");
        c.subscribe("mqttfirewall");
        c.receiveMessages();

    }
}
