package com.mqttfw.proxy.demo;

import java.util.Collection;

import org.json.JSONObject;

import com.mqttfw.model.Environment;
import com.mqttfw.model.MQTTPacket;
import com.mqttfw.model.expressions.ClockVariable;
import com.mqttfw.model.expressions.Variable;
import com.mqttfw.model.expressions.TimedObject.TimedObjectImplementation;
import com.mqttfw.model.statements.Statement;
import com.mqttfw.proxy.demo.FallModel.ACC_STATES;
import com.mqttfw.proxy.demo.FallModel.FallEffect;

public class UpdateStateAcceleration implements Statement {

    String sensorName;
    // String stateVarName;
    Variable stateVar;
    Variable payload;
    ClockVariable timerVar;
    
    public UpdateStateAcceleration(String sensorName, String stateVarName, String timerVarName, String payload) {
        this.sensorName = sensorName;
        this.stateVar = new Variable(stateVarName);
        this.timerVar = new ClockVariable(timerVarName);
        this.payload = new Variable(payload);
    }


    @Override
    public Environment apply(Environment env) {

        JSONObject payload = (JSONObject)this.payload.eval(env);
        String sensorName = payload.getString("id");
        if (sensorName.equals(this.sensorName))
        {
            Double value = payload.getDouble("value");
            String currState = (String)this.stateVar.eval(env);
            long timerValue = (Long)this.timerVar.eval(env);

            FallEffect<ACC_STATES> eff = new FallModel().getAccelerationState(ACC_STATES.valueOf(currState), timerValue, value);
            
            if (eff.resetClock) {
                env.resetClockVariable(this.timerVar.getName());
                System.out.println("UPDATE : " + this.stateVar.getName() + " : " + currState + " -> " + eff.state.name() + " ; " + this.timerVar.getName() + " := 0");
            }
            else 
            {
                System.out.println("UPDATE : " + this.stateVar.getName() + " : " + currState + " -> " + eff.state.name() + " ; ");
            }
            env.setValue(this.stateVar.getName(), eff.state.name());
            
        }

        return env;
    }

    public String toString() {
        return "UpdateStateAcceleration(" + this.sensorName + "," + this.stateVar + "," + this.timerVar + "," + this.payload + ")";
    }
    
}
