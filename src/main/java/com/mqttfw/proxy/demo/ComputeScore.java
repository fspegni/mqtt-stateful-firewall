package com.mqttfw.proxy.demo;

import com.mqttfw.model.Environment;
import com.mqttfw.model.expressions.Expression;
import com.mqttfw.model.expressions.Variable;
import com.mqttfw.proxy.demo.FallModel.ACC_STATES;
import com.mqttfw.proxy.demo.FallModel.IMAGE_STATES;

public class ComputeScore extends Expression {

    Variable varStateCamera;
    Variable varStateSmartWatch;
    Variable varStateSmartPhone;

    public ComputeScore(String varStateCamera, String varStateSmartWatch, String varStateSmartPhone) {
        this.varStateCamera = new Variable(varStateCamera);
        this.varStateSmartWatch = new Variable(varStateSmartWatch);
        this.varStateSmartPhone = new Variable(varStateSmartPhone);
    }


    @Override
    public Object eval(Environment env) {
        String cameraState = (String)this.varStateCamera.eval(env);
        String smartphoneState = (String)this.varStateSmartPhone.eval(env);
        String smartwatchState = (String)this.varStateSmartWatch.eval(env);

        double res = 0.0; //0.0f;
        if (cameraState == IMAGE_STATES.FELT.name()) {
            res += 0.5;
        }

        if (smartphoneState == ACC_STATES.FELT.name()) {
            res += 0.25;
        }

        if (smartwatchState == ACC_STATES.FELT.name()) {
            res += 0.25;
        }

        System.out.println("Filter: Score: " + res);

        return res;
    }
    

    public String toString() {
        return "ComputeScore(" + this.varStateCamera.toString() + "," + this.varStateSmartWatch.toString() + "," + this.varStateSmartPhone.toString() + ")";
    }
}
