package com.mqttfw.proxy.demo;

import java.net.ProtocolException;
import java.util.Arrays;

import com.mqttfw.model.MQTTPacket;
import com.mqttfw.model.MQTTPacket.RemainingLength;

import org.fusesource.hawtbuf.Buffer;
import org.fusesource.mqtt.codec.CONNECT;
import org.fusesource.mqtt.codec.MQTTFrame;
import org.fusesource.mqtt.codec.MessageSupport;

public class TestEncoding {

    public static void main(String[] args) {
        for (int i=0; i<1; i++) {
            doTest();
        }
    
    }
    public static void doTest() {

        byte[] buffer = { 16, 34, 0, 6, 77, 81, 73, 115, 100, 112, 3, 2, 0, 30, 0, 20, 55, 102, 48, 48, 48, 48, 48, 49, 97, 50, 102, 97, 54, 49, 101, 53, 56, 56, 50, 55 };

        System.out.println("Buffer size: " + buffer.length);
        //MQTT mqtt = new MQTT();
        Buffer b = new Buffer(Arrays.copyOfRange(buffer, 2, buffer.length));
        MessageSupport.Message m = new CONNECT();
        
        //MQTTPacket p;
        try {
            MQTTFrame f = new MQTTFrame(b);
		    f.header(buffer[0]);

            RemainingLength rl = MQTTPacket.decodeRemainingLength(f);
            System.out.println("Decoded remaining length: " + rl.getValue() + " (" + rl.getSize() + " bytes)");

            m.decode(f);
            System.out.println("Attempt 1 OK!");

            //p = new MQTTPacket(buffer[0], Arrays.copyOfRange(buffer, 1, buffer.length-1));
            //System.out.println("Pacchetto: " + p.toString());
        } catch (ProtocolException e) {
            System.err.println("Error during attempt 1");
            e.printStackTrace();
        }

        System.out.println("Attempt 2 ...");

        try {
            MQTTPacket p2 = new MQTTPacket(buffer);
            System.out.println("Attempt 2 decoding OK!");

            MQTTPacket.RemainingLength rl = MQTTPacket.decodeRemainingLength(buffer, 1);

            System.out.println("Decoded remaining length: " + rl.getValue() + " (" + rl.getSize() + " bytes)");

            byte[] serialized = p2.serialize();
            if (serialized.length != buffer.length) {
                throw new Exception("Serialized buffer has different size");
            }

            for (int i=0; i<serialized.length; i++) {
                if (serialized[i] != buffer[i]) {
                    throw new Exception("Byte at position " + i + " changed!!!");
                }
            }

            byte[] b1 = p2.encodeRemainingLength(127);
            byte[] b2 = p2.encodeRemainingLength(1000);
            byte[] b3 = p2.encodeRemainingLength(1000000);

            System.out.println("Attempt 2 OK");
            
        } catch (Exception e) {
            System.err.println("Error during attempt 2: " + e.getMessage());
            e.printStackTrace(System.err);
        }

    }
}
