package com.mqttfw.proxy.demo;

import com.mqttfw.filter.Firewall;
import com.mqttfw.proxy.NakovForwardServer;

public class StandaloneProxy {

    public static void main(String[] args) {
        Firewall f = new Firewall();
        NakovForwardServer server = new NakovForwardServer(f);
        try {
            server.readSettings();
            server.startForwardServer();
        } catch (Exception e) {
            System.err.println("Error starting server: " + e.getMessage());
            e.printStackTrace(System.err);
        }
        
    }
}
