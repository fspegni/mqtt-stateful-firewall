package com.mqttfw.proxy.demo;

import java.net.InetAddress;

import com.mqttfw.filter.Firewall;
import com.mqttfw.model.Environment;
import com.mqttfw.proxy.NakovForwardServer;
import com.mqttfw.proxy.NakovForwardServer.ServerDescription;

public abstract class Scenario {

    protected NakovForwardServer server;
    
    // override this in order to populate your rules and instantiate your concrete scenario
    public abstract void setFirewallRules(Firewall firewall);

    public void setupEnvironment(Environment env) {

    }

    public void startProxy(Firewall f) {
        server = new NakovForwardServer(f); 

        try {               
            server.readSettings();

            ServerDescription[] serverDescriptions = server.getServersList();

            for (ServerDescription sd : serverDescriptions) {
                System.out.println("Forwarding to server: " + sd.host + " : " + Integer.toString(sd.port));
            }

        } catch (Exception err) {
            System.err.println("Unable to read settings: " + err.getMessage());
            err.printStackTrace(System.err);
            System.exit(2);
        }

        Thread t = new Thread() {
            public void run() {

                try {
                    server.startForwardServer();
                } catch (Exception e) {
                    System.err.println("Error launching server: " + e.getMessage());
                    e.printStackTrace(System.err);
                    System.exit(3);
                }
            }
        };

        try {
            t.start();
        } catch (Exception err) {
            System.err.println("Error launching server thread: " + err.getMessage());
            err.printStackTrace(System.err);
            System.exit(1);
        }

    }

    public void startPublishers() {
        try {

            StandaloneClient c = new StandaloneClient("the_publisher", InetAddress.getLocalHost().getHostAddress(), 1883);
            
            System.out.println("Connecting publisher ...");
            c.connect();

            Runnable r = new Runnable() {

                @Override
                public void run() {

                    for (int i=0; i<50; i++) {
                        c.publish("mqttfirewall", "Client message " + i);
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            System.err.println("Error during sleep: " + e.getMessage());
                            e.printStackTrace();
                        }    
                    }
        
                    if (server != null) {
                        server.interrupt();
                    }
                        
                }

            };
            Thread t = new Thread(r); 

            t.start();

            System.out.println("Client ending ...");
        } catch (Exception err) {
            System.err.println("Error launching publisher: "  + err.getMessage());
            err.printStackTrace(System.err);

        }
        
    }

    public void startSubscribers() {
        try {
            StandaloneClient c = new StandaloneClient("the_subscriber", InetAddress.getLocalHost().getHostAddress(), 1883);
                
            System.out.println("Connecting ...");
            c.connect();
            
            c.subscribe("mqttfirewall");

            Runnable r = new Runnable() {

                @Override
                public void run() {
                    c.receiveMessages();
                }};

            Thread t = new Thread(r);
            t.start();

        } catch (Exception err) {
            System.err.println("Error launching subscriber: "  + err.getMessage());
            err.printStackTrace(System.err);

        }
    }

    public void run(String[] args) {

        Firewall f = new Firewall();
        Environment e = f.getEnvironment();

        this.setupEnvironment(e);
        this.setFirewallRules(f);

        this.startProxy(f);   
        this.startSubscribers();     
        this.startPublishers();

        
        
    }

}
