package com.mqttfw.proxy.demo;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

import org.fusesource.mqtt.codec.PUBLISH;

import com.mqttfw.filter.Firewall;
import com.mqttfw.model.Environment;
import com.mqttfw.model.Rule;
import com.mqttfw.model.RulePeriodic;
import com.mqttfw.model.expressions.Constant;
import com.mqttfw.model.expressions.GTE;
import com.mqttfw.model.expressions.HasType;
import com.mqttfw.model.expressions.Len;
import com.mqttfw.model.expressions.Negate;
import com.mqttfw.model.expressions.PacketHasType;
import com.mqttfw.model.expressions.ReadPacket;
import com.mqttfw.model.expressions.ReadPayload;
import com.mqttfw.model.expressions.Tautology;
import com.mqttfw.model.expressions.TimedObject;
import com.mqttfw.model.expressions.TimedObjectTimer;
import com.mqttfw.model.expressions.TimedObjectValue;
import com.mqttfw.model.statements.Append;
import com.mqttfw.model.statements.Assignment;
import com.mqttfw.model.statements.CopyCat;
import com.mqttfw.model.statements.ForEach;
import com.mqttfw.model.statements.IfThenElse;
import com.mqttfw.model.statements.Remove;
import com.mqttfw.model.statements.Sequence;


public class ScenarioTimeWindow extends Scenario {

    int timeWindow = 2000;
    int taskPeriodicity = 500;
    int fallRatio = 90;
    int securityThreshold = 2;

    @Override
    public void setFirewallRules(Firewall f) {

        // just one simple rule: accept everything and pass it through
        Rule r0 = new Rule(new Negate(new PacketHasType(PUBLISH.TYPE)), new CopyCat(), true);
        Rule r1 = new Rule(new Tautology(), new Assignment("last", new ReadPacket()));

        Rule r2 = new Rule(new HasType(new ReadPayload("last"), "java.lang.Double"),
            new IfThenElse(new GTE(new ReadPayload("last"), new Constant(fallRatio)), 
                new Append("hiValues", new TimedObject("last")),
                new CopyCat(null, "last")));

        Rule r3 = new RulePeriodic(new Tautology(), new ForEach("hiValues", "curr", new IfThenElse(
            new GTE(new TimedObjectTimer("curr"), new Constant(timeWindow)),
                new Sequence( 
                    new IfThenElse(
                        new GTE(new Len("hiValues"), new Constant(securityThreshold)),
                        new CopyCat(null, new TimedObjectValue("curr"))
                    ),
                    new Remove("curr")
                )
        )), taskPeriodicity);


        f.addRule(r0, "r0");
        f.addRule(r1, "r1");
        f.addRule(r2, "r2");
        f.addRule(r3, "r3");
    }

    @Override
    public void setupEnvironment(Environment env) {
        //env.setVerbose();

        env.addVariable("last");
        env.addVariable("hiValues", new ArrayList<Object>());
    }

    @Override
    public void startPublishers() {
        try {

            StandaloneClient c = new StandaloneClient();
            
            System.out.println("Connecting ...");
            c.connect();

            for (int i=0; i<50; i++) {
                // generate random values in the interval [0,100]
                int someValue = ThreadLocalRandom.current().nextInt(0, 101); 

                // publish a string and an integer
                c.publish("mqttfirewall", "Client message " + i);
                c.publish("mqttfirewall", String.valueOf(someValue));

                // generate a random delay and then sleep
                int someDelay = ThreadLocalRandom.current().nextInt(100, 300);
                Thread.sleep(someDelay);    
            }

            if (this.server != null) {
                this.server.interrupt();
            }
            
            System.out.println("Client ending ...");
        } catch (Exception err) {
            System.err.println("Error launching client: "  + err.getMessage());
            err.printStackTrace(System.err);

        }
        
    }


    public static void main(String[] args) {
        Scenario s = new ScenarioTimeWindow();

        s.run(args);
    }
    
}
