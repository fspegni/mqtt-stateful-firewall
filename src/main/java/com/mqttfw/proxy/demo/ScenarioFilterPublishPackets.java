package com.mqttfw.proxy.demo;

import com.mqttfw.filter.Firewall;
import com.mqttfw.model.Environment;
import com.mqttfw.model.Rule;
import com.mqttfw.model.expressions.And;
import com.mqttfw.model.expressions.Constant;
import com.mqttfw.model.expressions.Diff;
import com.mqttfw.model.expressions.Equal;
import com.mqttfw.model.expressions.Negate;
import com.mqttfw.model.expressions.PacketHasType;
import com.mqttfw.model.expressions.Variable;
import com.mqttfw.model.statements.Assignment;
import com.mqttfw.model.statements.CopyCat;

import org.fusesource.mqtt.codec.PUBLISH;

public class ScenarioFilterPublishPackets extends Scenario {

    @Override
    public void setFirewallRules(Firewall f) {
        
        Rule r0 = new Rule(new Negate(new PacketHasType(PUBLISH.TYPE)), new CopyCat(), true);
        Rule r1 = new Rule(new PacketHasType(PUBLISH.TYPE), new Assignment("alternate", new Diff(new Constant(1), new Variable("alternate"))));
        Rule r2 = new Rule(new And(new PacketHasType(PUBLISH.TYPE), new Equal(new Variable("alternate"), new Constant(1))), new CopyCat());
        
        f.addRule(r0, "r0");
        f.addRule(r1, "r1");
        f.addRule(r2, "r2");
    }

    @Override
    public void setupEnvironment(Environment env) {
        env.addVariable("alternate", 0);
        env.setVerbose();
    }

    public static void main(String[] args) {
        Scenario s = new ScenarioFilterPublishPackets();

        s.run(args);
    }
    
}
