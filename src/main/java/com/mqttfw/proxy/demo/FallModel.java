package com.mqttfw.proxy.demo;

import java.util.concurrent.ThreadLocalRandom;

class FallModel {

    enum ACC_STATES {
        NORMAL,
        FREE_FALL,
        IMPACT,
        FELT        
    };

    enum IMAGE_STATES {
        NORMAL,
        FELT
    };

    static Double thff = 0.7;
    static Double thpi = 1.15;
    static Double thi = 1.5;


    private static double line(double x, double xA, double yA, double xB, double yB) {
        double d = (xA - xB);
        return x * ((yA - yB)/d) + (xA * yB - yA * xB)/d; 
    }


    public static double getAcceleration(long delta) {
        //double someGaussianValue = ThreadLocalRandom.current().nextGaussian();
        
        // int fallId = this.fallIdentifier();
        double value = 1;

        // if (fallId >= 0) {
            // when fall identified by fallId is happening, the acceleration depends on the 
            // time-distance between now and the time of the fall
            // long now = readClock();
            // long delta = now - startClock - this.fallsClocks[fallId];
            if (delta < -200 || delta >= 500) {
                // far away from the fall
                value += ThreadLocalRandom.current().nextDouble(-0.01, 0.01); 
            } else if (delta >= -200 && delta < -100) {
                // F1 : acceleration = -200 - 199.4/100 * delta
                value += line(delta, -200, 0, -100, -0.7);
            } else if (delta >= -100 && delta < 0) {
                // F2
                value += line(delta, -100, -0.7, 0, 0);
            } else if (delta >= 0 && delta < 100) {
                // F3
                value += line(delta, 0, 0, 100, 2.3);
            } else if (delta >= 100 && delta < 200){
                // F4
                value += line(delta, 100, 2.3, 600, -0.4);
            } else if (delta >= 200 && delta < 500) {
                // F5
                value += line(delta, 600, -0.4, 900, 0);
            } else {
                throw new RuntimeException("Piecewise function not well defined for value: " + Long.toString(delta));
            }
            System.out.println("Acceleration : " + Double.toString(value) + " Delta : " + delta); //  + " Now : " + Long.toString(now) + " Start : " + Long.toString(startClock) + " Value : " + Double.toString(1 + value));

        // } else {
        //     // when not falling, the acceleration is more or less 1G
        //     value = ThreadLocalRandom.current().nextDouble(-0.01, 0.01); 
        // }

        return value; 
    }

    public class FallEffect<T> {
        T state;
        Boolean resetClock = false;

        public FallEffect(T initialState) {
            this.state = initialState;
        }
    }


    public FallEffect<ACC_STATES> getAccelerationState(ACC_STATES currState, long delta, double acceleration) {
        //ACC_STATES new_state;

        FallEffect<ACC_STATES> res = new FallEffect<ACC_STATES>(currState);

        if (currState == ACC_STATES.NORMAL && acceleration < thff) {
            //new_state = ACC_STATES.FREE_FALL;
            //sp_clock_t = Clock.systemDefaultZone().millis();
            res.state =  ACC_STATES.FREE_FALL;
            res.resetClock = true;
        } else if (currState == ACC_STATES.FREE_FALL) {
            if (delta > 400) {
                // new_state = ACC_STATES.NORMAL;
                res.state = ACC_STATES.NORMAL;
                res.resetClock = false;

            } else if (acceleration > thi) {
                // new_state = ACC_STATES.IMPACT;
                // sp_clock_t = Clock.systemDefaultZone().millis();
                res.state =  ACC_STATES.IMPACT;
                res.resetClock = true;
            }
        } else if (currState == ACC_STATES.IMPACT) {
            if (acceleration > thpi && delta > 1000) {
                // new_state = ACC_STATES.NORMAL;
                res.state = ACC_STATES.NORMAL;
                res.resetClock = false;
            } else if (acceleration < thpi && delta > 2500) {
                // new_state = ACC_STATES.FELT;
                res.state = ACC_STATES.FELT;
                res.resetClock = false;
            }
        }

        return res;
    }

    public FallEffect<IMAGE_STATES> getImageState(IMAGE_STATES currState, boolean felt) {
        FallEffect<IMAGE_STATES> res = new FallEffect<IMAGE_STATES>(IMAGE_STATES.NORMAL);

        if (felt) {
            res.state = IMAGE_STATES.FELT;
        } else {
            res.state = IMAGE_STATES.NORMAL;
        }

        return res;
    }
}