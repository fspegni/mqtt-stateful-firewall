package com.mqttfw.model;

import com.mqttfw.model.expressions.Expression;
import com.mqttfw.model.statements.Statement;


public class RulePeriodic extends Rule {
	boolean isFinal = false;
	String name = "<N/N>";
	
	int period;

	public RulePeriodic(Expression cond, Statement thenBranch, int period) {
		super(cond, thenBranch, false);
		this.period = period;
	}

	public int getPeriod() {
		return this.period;
	}
	
}
