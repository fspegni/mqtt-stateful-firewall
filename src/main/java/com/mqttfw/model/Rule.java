package com.mqttfw.model;

import com.mqttfw.model.expressions.Expression;
import com.mqttfw.model.statements.IfThenElse;
import com.mqttfw.model.statements.Statement;


public class Rule extends IfThenElse {
	boolean isFinal = false;
	String name = "<N/N>";
	
	public Rule(Expression cond, Statement thenBranch, boolean isFinal) {
		super(cond, thenBranch);
		this.isFinal = isFinal;
	}

	public Rule(Expression cond, Statement thenBranch) {
		super(cond, thenBranch);
		this.isFinal = false;
	}

	

	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}

	public boolean isEnabled(Environment env) {
		Object res = this.getCondition().eval(env);
		
		return (res instanceof Boolean && (Boolean)res == true);
	}

	
	public boolean isFinal() {
		return this.isFinal;
	}

	@Override
	public synchronized Environment apply(Environment env) {
		if (env.isVerbose())
		{
			System.out.println("Apply rule: " + this.name);
		}
		
		return super.apply(env);
	}

	public String toString() {
		String ruleName = this.name;
		if (this.isFinal) {
			ruleName += " [F]";
		}
		return ruleName + " : " + super.toString(); 
	}
}
