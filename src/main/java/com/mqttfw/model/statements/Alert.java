package com.mqttfw.model.statements;

import com.mqttfw.model.Environment;

public class Alert implements Statement {

    public enum LEVEL {
        DEBUG,
        INFO,
        WARN,
        ERROR
    };

    String message;
    LEVEL level;

    public Alert(LEVEL level, String message) {
        this.level = level;
        this.message = message;
    }
    
    @Override
    public Environment apply(Environment env) {
        System.out.println(this.level.name() + " : " + this.message);
        return env;
    }
    
    public String toString() {
        return "Alert(" + this.level.name() + "," + this.message + ")";
    }
}
