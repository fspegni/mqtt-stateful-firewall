package com.mqttfw.model.statements;

import java.util.ArrayList;

import com.mqttfw.model.Environment;
import com.mqttfw.model.expressions.Expression;

public class Append implements Statement {

    String varName;
    Expression value;

    public Append(String varName, Expression value) {
        this.value = value;
        this.varName = varName;
    }

    public Environment apply(Environment env) {
        Object newItem = this.value.eval(env);
        ArrayList<Object> theList = (ArrayList<Object>)env.getValue(this.varName);

        synchronized (theList) {
            theList.add(newItem);
        }
        
        env.setValue(this.varName, theList);
        return env;
    }

    public String toString() {
        return "Append(" + this.varName + "," + this.value.toString() + ")";
    }
}
