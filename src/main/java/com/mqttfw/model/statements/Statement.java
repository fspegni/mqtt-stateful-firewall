package com.mqttfw.model.statements;

import com.mqttfw.model.Environment;

public interface Statement {
	public Environment apply(Environment env);
}
