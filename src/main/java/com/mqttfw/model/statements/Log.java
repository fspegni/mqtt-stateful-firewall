package com.mqttfw.model.statements;

import com.mqttfw.model.Environment;
import com.mqttfw.model.expressions.Expression;

public class Log implements Statement {

    Expression exp;
    String extra;

    public Log(Expression exp, String extra) {
        this.exp = exp;
        this.extra = extra;
    }

    @Override
    public Environment apply(Environment env) {
        Object value = this.exp.eval(env);
        System.out.println(this.extra + value.toString());
        return env;
    }
    
}
