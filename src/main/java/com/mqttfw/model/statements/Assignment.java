package com.mqttfw.model.statements;

import com.mqttfw.model.Environment;
import com.mqttfw.model.expressions.Expression;
import com.mqttfw.model.expressions.Variable;

public class Assignment implements Statement {

	Variable lhs;
	Expression rhs;

	public Assignment(String var, Expression exp) {
		this(new Variable(var), exp);
	}

	public Assignment(Variable var, Expression exp) {
		this.lhs = var;
		this.rhs = exp;
	}
	
	public Environment apply(Environment env) {
		env.setValue(lhs.getName(), this.rhs.eval(env));
		return env;
	}

	public String toString() {
		return this.lhs.toString() + " := " + this.rhs.toString();
	}
}
