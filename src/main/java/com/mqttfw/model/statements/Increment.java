package com.mqttfw.model.statements;

import com.mqttfw.model.Environment;
import com.mqttfw.model.expressions.Variable;

public class Increment implements Statement {

	Variable var;

	public Increment(Variable var) {
		this.var = var;
	}
	
	public Environment apply(Environment env) {
		Double value = Double.valueOf(env.getValue(this.var.getName()).toString());
		env.setValue(this.var.getName(), (Double)(value + 1));
		return env;
	}

	public String toString() {
		return this.var.toString() + "++";
	}
}
