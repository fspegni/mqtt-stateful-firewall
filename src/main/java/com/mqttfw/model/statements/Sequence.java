package com.mqttfw.model.statements;

import java.util.ArrayList;

import com.mqttfw.model.Environment;

public class Sequence implements Statement {

	Statement[] statements;
	
	public Sequence(Statement ...statements) {
		this.statements = statements;
	}
	
	public Environment apply(Environment env) {
		for (Statement curr : this.statements) {
			env = curr.apply(env);
		}
		
		return env;
	}

	public String toString() {
		ArrayList<String> parts = new ArrayList<String>();

		for (Statement s : this.statements) {
			parts.add(s.toString());
		}

		return String.join("; ", parts);
	}
}
