package com.mqttfw.model.statements;

import java.util.ArrayList;

import com.mqttfw.model.Environment;

public class NewList {

    String varName;

    public NewList(String varName) {
        this.varName = varName;
    }

    public Environment apply(Environment env) {
        env.setValue(this.varName, new ArrayList<>());
        return env;
    }

    public String toString() {
        return "NewList()";
    }
}
