package com.mqttfw.model.statements;

import com.mqttfw.model.Environment;
import com.mqttfw.model.expressions.Expression;
import com.mqttfw.model.expressions.Or;

public class IfThenElse implements Statement {

	protected Expression cond;
	protected Statement thenBranch;
	protected Statement elseBranch;
	
	public IfThenElse(Expression cond, Statement thenBranch, Statement elseBranch) {
		this.cond = cond;
		this.thenBranch = thenBranch;
		this.elseBranch = elseBranch;
	}
	
	public IfThenElse(Expression cond, Statement thenBranch) {
		this.cond = cond;
		this.thenBranch = thenBranch;
	}
	
	public Environment apply(Environment env)  {
		Boolean value = (Boolean)this.cond.eval(env);
		
		Environment res = env;
		
		if (value) {
			res = this.thenBranch.apply(env);
		} else if (this.elseBranch != null) {
			res = this.elseBranch.apply(env);
		}
		
		return res;
	}
	
	public Expression getCondition() {
		return this.cond;
	}
	
	public Statement getThenBranch() {
		return this.thenBranch;
	}
	
	public Statement getElseBranch() {
		return this.elseBranch;
	}
	
	public String toString() {
		String strElseBranch = "";

		if (this.elseBranch != null) {
			strElseBranch = " else { " + this.elseBranch.toString() + " }";
		} 

		return "if (" + this.cond.toString() + ") { " + this.thenBranch.toString() + "}" + strElseBranch;
	}


}
