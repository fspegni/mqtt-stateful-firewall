package com.mqttfw.model.statements;

import java.time.Clock;

import com.mqttfw.model.Environment;
import com.mqttfw.model.expressions.ClockVariable;

public class ClockReset implements Statement {

    static Clock c = Clock.systemDefaultZone();
    ClockVariable var;

    public ClockReset(String varName) {
        this.var = new ClockVariable(varName);
    }

    @Override
    public Environment apply(Environment env) {

        env.setValue(this.var.getName(), c.millis());
        return env;
    }
    
    public String toString() {
        return "ClockReset(" + this.var.toString() + ")";
    }
}
