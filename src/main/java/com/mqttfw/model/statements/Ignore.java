package com.mqttfw.model.statements;

import java.util.Queue;

import com.mqttfw.model.Environment;
import com.mqttfw.model.MQTTPacket;

public class Ignore implements Statement {

	public Environment apply(Environment env) {
		Queue<MQTTPacket> qWaiting = env.getWaitingQueue(); 
		
		while (! qWaiting.isEmpty()) {
			qWaiting.remove();
		}
		
		return env;
		
	}

	public String toString() {
		return "Ignore()";
	}
}
