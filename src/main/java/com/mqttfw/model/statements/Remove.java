package com.mqttfw.model.statements;

import java.util.Iterator;

import com.mqttfw.model.Environment;

public class Remove implements Statement {

    protected String iteratorName;

    public Remove(String iteratorName) {
        this.iteratorName = iteratorName;
    }

    @Override
    public Environment apply(Environment env) {
        Iterator<?> it = env.getIterator(this.iteratorName);
        it.remove();
        return env;
    }
    
    public String toString() {
        return "Remove(" + this.iteratorName + ")";
    }
}
