package com.mqttfw.model.statements;

import java.util.ArrayList;
import java.util.Collection;
import com.mqttfw.model.Environment;
import com.mqttfw.model.MQTTPacket;
import com.mqttfw.model.expressions.Expression;
import com.mqttfw.model.expressions.Variable;
import com.mqttfw.model.expressions.TimedObject.TimedObjectImplementation;

public class CopyCat implements Statement {
	Expression target;
	Expression source;

	public CopyCat(Expression target){
		this.target = target;
	};

	public CopyCat(String target) {
		this.target = new Variable(target);
	}

	public CopyCat(Expression target, Expression source) {
		this.source = source;
		this.target = target;
	}

	public CopyCat(String target, String source) {
		if (source != null) {
			this.source = new Variable(source);
		}

		if (target != null) {
			this.target = new Variable(target);
		}
	}

	public CopyCat(){
	};

	private Collection<MQTTPacket> getPacketsFromVar(Expression exp, Environment env) {
		if (exp == null) {
			throw new NullPointerException("Expected a non-null expression");
		}
		Collection<MQTTPacket> result = new ArrayList<MQTTPacket>();
		// Object packets = env.getValue(name);
		Object packets = exp.eval(env);
		if (packets instanceof MQTTPacket) {
			// result = new ArrayList<MQTTPacket>();
			result.add((MQTTPacket)packets);
		} else if (packets instanceof Collection) {
			Collection temp = (Collection) packets;
			for (Object curr : temp) {
				if (curr instanceof MQTTPacket) {
					result.add((MQTTPacket)curr);
				} else if (curr instanceof TimedObjectImplementation && ((TimedObjectImplementation)curr).getValue() instanceof MQTTPacket) {
					result.add((MQTTPacket)((TimedObjectImplementation)curr).getValue());
				} else {
					System.err.println("Skip timed object in CopyCat: it does not contain an MQTTPacket");
				}
			}
		} else {
			throw new RuntimeException("Error in CopyCat: don't know how to extract MQTTPacket's from expression (" + exp + ")");
		}

		return result;
		
	}

	public Environment apply(Environment env) {
		// Queue<MQTTPacket> qWaiting = env.getWaitingQueue();
		Collection<MQTTPacket> qTarget;

		if(target == null){
			qTarget = env.getFilteredQueue();
		} else {
			//qTarget = (Collection<MQTTPacket>) env.getValue(this.target);
			qTarget = getPacketsFromVar(this.target, env);
		}

		assert(qTarget != null);

		Collection<MQTTPacket> qSource;
		if (source == null) {
			qSource = env.getWaitingQueue();
		} else {
			//qSource = (Collection<MQTTPacket>) env.getValue(this.source);
			qSource = getPacketsFromVar(this.source, env);
		}

		// Iterator<MQTTPacket> it = qSource.iterator();
		for (MQTTPacket curr : qSource) {
		// while (it.hasNext()) {
			// qTarget.add(it.next());
			//it.remove(); // 
		// }
			qTarget.add(curr);
		}
		qSource.clear();
		
		return env;
		
	}

	public String toString() {
		String strSource = "";
		if (this.source != null) {
			strSource = this.source.toString();
		}

		String strTarget = "";
		if (this.target != null) {
			strTarget = this.target.toString();
		}
		
		return "CopyCat(" + strSource + " -> " + strTarget + ")";
	}
}
