package com.mqttfw.model.statements;

import com.mqttfw.model.Environment;
import com.mqttfw.model.MQTTPacket;
import com.mqttfw.model.expressions.Variable;
import org.fusesource.mqtt.codec.PUBLISH;

import java.nio.charset.StandardCharsets;

public class IncrementPayload implements Statement {
    Variable var;
    Double value = 0.0; //0f;

    public IncrementPayload(Variable var) {
        this.var = var;
    }

    @Override
    public Environment apply(Environment env) {
        MQTTPacket packet = env.getWaitingQueue().element();
        PUBLISH p = (PUBLISH)packet.msg;
        byte[] b = p.payload().trim().toByteArray();
        String s = new String(b, StandardCharsets.UTF_8);
        int num = Integer.parseInt(s);
        value = value + num;
        env.setValue(this.var.getName(), value);
        return env;
    }
}
