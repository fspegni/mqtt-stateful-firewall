package com.mqttfw.model.statements;

import java.util.Collection;
import java.util.Iterator;

import com.mqttfw.model.Environment;
import com.mqttfw.model.expressions.Variable;

public class ForEach implements Statement {

    protected Variable collection;
    protected String itemName;
    protected Statement body;

    public ForEach(String collectionName, String itemName, Statement body) {
        this.collection = new Variable(collectionName);
        this.itemName = itemName;
        this.body = body;
    }

    @Override
    public Environment apply(Environment env) {
        
        try {
            Collection<?> c = (Collection<?>)this.collection.eval(env);

            // use iterators (instead of the Java forEach) because the body.apply(...) may
            // contain a remove opereation
            Iterator<?> it = c.iterator();
            
            int num = 0;
            while(it.hasNext()) {
                Object curr = it.next();
                if (env.isVerbose()) {
                    System.out.println("Iterate (" + num + "): " + this.itemName + " -> " + curr);   
                }
                env.addIterator(this.itemName, it);
                env.addOrSetVariable(this.itemName, curr);
                try {
                    env = this.body.apply(env);
                } catch (Exception e) {
                    throw e;
                } finally {
                    env.removeIterator(this.itemName);
                }
                num++;
            }
            if (env.isVerbose()) {
                System.out.println("ForEach terminated after " + num + " iterations");
            }

        } catch (ClassCastException e) {
            e.printStackTrace();
            throw new RuntimeException("ForEach does not work because variable '" + this.collection.getName() + "' does not refer to a collection");
        } 


        return env;
    }
    
    public String toString() {
        return "for (" + this.itemName + " : " + this.collection.toString() + ") { " + this.body.toString() + "}";
    }
}
