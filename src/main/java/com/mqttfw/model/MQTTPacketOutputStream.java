package com.mqttfw.model;

import java.io.IOException;
import java.io.OutputStream;
import java.net.ProtocolException;
import java.util.Queue;

public class MQTTPacketOutputStream extends OutputStream {

	Queue<MQTTPacket> queue = null; 
	
	public MQTTPacketOutputStream(Queue<MQTTPacket> queue) {
		if (queue == null) {
			throw new IllegalArgumentException("Expected a non-null queue");
		}
		this.queue = queue;
	}
	
	Queue<MQTTPacket> getQueue() {
		return this.queue;
	}
	
	@Override
	public void write(int arg0) throws IOException {
		// TODO we may enqueue the passed arg, and form a byte stream (it is not clear how to detect when the byte stream contains a full packet and can be "parsed"
		throw new IOException("TODO Cannot handle the write of a single byte on this stream");
	}
	
	@Override
	public void write(byte[] b) {
		MQTTPacket p;
		try {
			p = new MQTTPacket(b); // TODO was: new MQTTPacket(b[0], Arrays.copyOfRange(b, 1, b.length));
			synchronized (this.queue) {  
				this.queue.add(p);
				// awakes the threads waiting for new packets in the queue
				this.queue.notifyAll();
			}
		} catch (Exception e) {
			System.err.println("Error writing MQTT packet: " + e.getMessage());
			e.printStackTrace();
		}
		
	}

}
