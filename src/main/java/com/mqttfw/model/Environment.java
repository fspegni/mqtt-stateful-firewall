package com.mqttfw.model;

import java.io.OutputStream;
import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Queue;
import java.util.Timer;
import java.time.Clock;

public class Environment {
	
	
	public Map<String, Object> configuration = new HashMap<String, Object>();
	public Map<String, Iterator<?>> iterators = new HashMap<String, Iterator<?>>();
	public Queue<MQTTPacket> queueWaiting = new ArrayDeque<MQTTPacket>();
	public Queue<MQTTPacket> queueFiltered = new ArrayDeque<MQTTPacket>();

	public boolean verbose;

	protected OutputStream out;
	protected Timer timer;
	static Clock c = Clock.systemDefaultZone();

	public Environment() {
		//this.out = out;
		
		// _QUEUE_IN and _QUEUE_OUT are special variables:
		// * _QUEUE_IN is used by Rule's conditions in order to check
		//   whether they are enabled
		// * _QUEUE_OUT are modified by Rule's statements (if needed) 
		//   in order to determine what (filtered) messages any client 
		//   of the Firewall app can "see"
		this.addVariable("_QUEUE_WAITING", this.queueWaiting);
		this.addVariable("_QUEUE_FILTERED", this.queueFiltered);

		this.timer = new Timer();
	}

	public void addIterator(String name, Iterator<?> it) {
		this.assertIteratorDoesNotExist(name);
		this.iterators.put(name, it);
	}

	public void removeIterator(String name) {
		this.assertIteratorExists(name);

		this.iterators.remove(name);
	}

	public Iterator<?> getIterator(String name) {
		this.assertIteratorExists(name);
		return this.iterators.get(name);
	}

	public Timer getTimer() {
		return this.timer;
	}

	public void addClockVariable(String name) {
		addVariable(name, c.millis());
	}

	public long readClockVariable(String name, long now) {
		long value = (Long)getValue(name);
		return now - value;
	}

	public long readClockVariable(String name) {
		long now = c.millis();
		return readClockVariable(name, now);
	}

	public void resetClockVariable(String name) {
		setValue(name, c.millis());
	}
	
	public void addVariable(String name) {
		addVariable(name, null);
	}

	public void addVariable(String name, Object value) {
		this.assertVariableDoesNotExist(name);
		
		this.configuration.put(name, value);
	}
	
	public void setValue(String name, Object value) {
		if (! this.configuration.containsKey(name)) {
			System.err.println("Warning: variable '" + name + "' is initialized on the fly");
		}
		//this.assertVariableExists(name);
		
		this.configuration.put(name, value);
	}

	public void addOrSetVariable(String name, Object value) {
		this.configuration.put(name, value);
	}
	
	public Object getValue(String name) {
		this.assertVariableExists(name);
		
		return this.configuration.get(name);
	}
	
	public boolean hasVariable(String name) {
		return this.configuration.containsKey(name);
	} 

	public boolean hasIterator(String name) {
		return this.iterators.containsKey(name);
	} 



	public void assertVariableDoesNotExist(String name) {
		if (name == null) {
			throw new NullPointerException("Expected a non-null variable name");
		}
		if (this.hasVariable(name)) {
			throw new RuntimeException("Variable " + name +" already exists in the environment");
		}
	}

	public void assertVariableExists(String name) {
		if (name == null) {
			throw new NullPointerException("Expected a non-null variable name");
		}
		if (! this.hasVariable(name)) {
			throw new RuntimeException("Variable " + name +" does not exist in the environment");
		}
	}

	public void assertIteratorDoesNotExist(String name) {
		if (name == null) {
			throw new NullPointerException("Expected a non-null iterator name");
		}
		if (this.hasIterator(name)) {
			throw new RuntimeException("Iterator " + name +" already exists in the environment");
		}
	}

	public void assertIteratorExists(String name) {
		if (name == null) {
			throw new NullPointerException("Expected a non-null iterator name");
		}
		if (! this.hasIterator(name)) {
			throw new RuntimeException("Iterator " + name +" does not exist in the environment");
		}
	}


	public <T> T getCastedVariable(String name, Class<T> clazz) {
		this.assertVariableExists(name);

		Object obj = this.getValue(name);
		
		return clazz.cast(obj);
	}

	public Queue<MQTTPacket> getWaitingQueue() {
		this.assertVariableExists("_QUEUE_WAITING");
		return (Queue<MQTTPacket>)this.getValue("_QUEUE_WAITING");
	}
	
	public Queue<MQTTPacket> getFilteredQueue() {

		this.assertVariableExists("_QUEUE_FILTERED");
		return (Queue<MQTTPacket>)this.getValue("_QUEUE_FILTERED");
	}
	
	
	public Queue<MQTTPacket> getPacketQueueWaiting() {
		return this.queueWaiting;
	}
	
	public Queue<MQTTPacket> getPacketQueueFiltered() {
		return this.queueFiltered;
	}

	public void setVerbose() {
		this.verbose = true;
	}

	public boolean isVerbose() {
		return this.verbose;
	}

}
