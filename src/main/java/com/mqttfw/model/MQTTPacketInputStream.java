package com.mqttfw.model;

import java.io.IOException;
import java.io.InputStream;
import java.util.Queue;

public class MQTTPacketInputStream extends InputStream {

	Queue<MQTTPacket> queue = null;
	
	byte[] currPacket = null;
	int currPos = -1;
	boolean blocking;
	Object lock;
	
	public MQTTPacketInputStream(Queue<MQTTPacket> queue, Object lock) {
		if (queue == null) {
			throw new IllegalArgumentException("Must specify a non-null queue to instantiate");
		}
		this.queue = queue;
		this.blocking = (lock != null); //true; //blocking;
		this.lock = lock;
	}
	
	public MQTTPacketInputStream(Queue<MQTTPacket> queue) {
		this(queue, queue);
	}
	
	@Override
	public int read() throws IOException {
        throw new IOException("Not implemented yet!");
	} 
	
	@Override
	public int read(byte[] b) throws IOException {
		int res = -1;
		
		try {/*
			while (queue.size() == 0 && this.blocking) {
				// TODO use interrupts, instead of busy waiting
				//this.wait(1000);
				// System.err.println("Busy waiting: sleep 1 sec ...");
				Thread.sleep(1000);
			} */
			if (queue.size() == 0) {
				return 0;
			}
			
			MQTTPacket p = queue.remove();
			
			byte[] content = p.serialize();
			
			// copy the serialized packet into the passed buffer
			for (int pos=0; pos<content.length; pos++) {
				b[pos] = content[pos];
			}
			
			res = content.length;
		} catch (Exception e) {
			System.err.println("Error during read: " + e.getMessage());
			e.printStackTrace(System.err);
			res = -1;
			
		}
		
		return res;
	}
}
