package com.mqttfw.model;

import com.mqttfw.model.expressions.Expression;
import com.mqttfw.model.statements.Statement;


public class RuleDelay extends Rule {
	boolean isFinal = false;
	String name = "<N/N>";
	
	int delay;

	public RuleDelay(Expression cond, Statement thenBranch, int delay) {
		super(cond, thenBranch, false);
		this.delay = delay;
	}

	public int getDelay() {
		return this.delay;
	}
}
