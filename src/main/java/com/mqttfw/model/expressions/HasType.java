package com.mqttfw.model.expressions;

import com.mqttfw.model.Environment;

public class HasType extends Expression {
    protected int value;

    protected Expression arg;
    String typeName;

    public HasType(String varName, String typeName) {
        this.arg = new Variable(varName);
        this.typeName = typeName;
    }

    public HasType(Expression arg, String typeName){
        this.arg = arg;
        this.typeName = typeName;
    }

    @Override
    public Object eval(Environment env) {

        Object arg = this.arg.eval(env);

        if (arg == null) {
            throw new RuntimeException("Unable to retrieve value of argument or variable");
        }

        return arg.getClass().getName() == typeName;
    }
    
    public String toString() {
        return "HasType("  + this.arg.toString() + ")";
    }
}
