package com.mqttfw.model.expressions;

import com.mqttfw.model.Environment;
import org.json.JSONObject;

public class JSONField extends Expression {

    protected Expression arg;
    protected String fieldName;

    public JSONField(Expression arg, String fieldName) {
        this.arg = arg;
        this.fieldName = fieldName;
    }

    public JSONField(String varName, String fieldName) {
        this.arg = new Variable(varName);
        this.fieldName = fieldName;
    }

    @Override
    public Object eval(Environment env) {
        Object value = this.arg.eval(env);

        if (value == null) {
            throw new RuntimeException("This expression can be applied only to object of type JSONObject. Received : null");
        } else if (! (value instanceof JSONObject)) {
            throw new RuntimeException("This expression applies only to object of type JSONObject. Received: " + value.getClass().getName());
        }

        JSONObject job = (JSONObject)value;
        
        if (job.has(this.fieldName)) {
            return job.get(this.fieldName);
        } else {
            throw new RuntimeException("Cannot find key '" + this.fieldName + "'. Available keys are: " + job.keySet().toString());
        }


    }

    public String toString() {
        return this.arg.toString() + "[" + this.fieldName + "]";
    }
}
