package com.mqttfw.model.expressions;

import java.util.Queue;

import com.mqttfw.model.Environment;
import com.mqttfw.model.MQTTPacket;

public class PacketHasType extends Expression {

	protected byte type;
	
	public PacketHasType(byte type) {
		this.type = type;
	}
	
	public Object eval(Environment env) {
		
		Queue<MQTTPacket> q = env.getWaitingQueue();

		if (q.size() == 0) {
			throw new RuntimeException("The waiting queue is expected to be non-empty in order to apply this rule");
		}

		// con element prendi il primo elemento della waiting que, ma a noi serve l'ultimo scritto
		try {
			MQTTPacket first = q.element();

			return first.messageType() == this.type;
		} catch (Exception err) {
			System.err.println("Error picking element from queue: " + err.getMessage());
			err.printStackTrace();
			return false;
		}

		// return first.messageType() == this.type;
	}

	public String toString() {
        return "PacketHasType("  + String.valueOf(this.type) + ")";
    }

}
