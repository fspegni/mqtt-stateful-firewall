package com.mqttfw.model.expressions;

import com.mqttfw.model.Environment;

public class LTE extends Expression {

	protected Expression lhs;
	protected Expression rhs;
	
	public LTE(Expression lhs, Expression rhs) {
		this.lhs = lhs;
		this.rhs = rhs;
	}
	
	public Object eval(Environment env) {
		
		Comparable lvalue = (Comparable)this.lhs.eval(env);
		Comparable rvalue = (Comparable)this.rhs.eval(env);
		
		return lvalue.compareTo(rvalue) <= 0; // <= rvalue;
	}


	public String toString() {
		return this.lhs.toString()  + " <= " + this.rhs.toString();
	}
}
