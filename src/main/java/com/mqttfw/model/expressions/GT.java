package com.mqttfw.model.expressions;

import com.mqttfw.model.Environment;

public class GT extends Expression {

	protected Expression lhs;
	protected Expression rhs;
	
	public GT(Expression lhs, Expression rhs) {
		this.lhs = lhs;
		this.rhs = rhs;
	}

	public GT(String lhs, Expression rhs) {
		this(new Variable(lhs), rhs);
	}
	
	public Object eval(Environment env) {
		
		Comparable lvalue = (Comparable)this.lhs.eval(env); //.toString());
		Comparable rvalue = (Comparable)this.rhs.eval(env); //.toString());
		
		return lvalue.compareTo(rvalue) > 0; // > rvalue;
	}

	public String toString() {
		return this.lhs.toString()  + " > " + this.rhs.toString();
	}
}
