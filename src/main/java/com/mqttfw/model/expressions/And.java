package com.mqttfw.model.expressions;

import java.util.ArrayList;

import com.mqttfw.model.Environment;

public class And extends Expression {
    
    Expression[] arguments;

    public And(Expression ...arguments) {
        this.arguments = arguments;
    }

    @Override
    public Object eval(Environment env) {
        for (Expression e : this.arguments) {
            if ((Boolean) e.eval(env) == false) {
                return false;
            }
        }

        return true;
    }

    public String toString() {
        ArrayList<String> parts = new ArrayList<>();
        
        for (Expression e : this.arguments) {
            parts.add(e.toString());
        }
        return String.join(" && ", parts);
    }


}
