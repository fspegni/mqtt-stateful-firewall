package com.mqttfw.model.expressions;

import com.mqttfw.model.Environment;

public class Equal extends Expression {

	protected Expression lhs;
	protected Expression rhs;
	
	public Equal(Expression lhs, Expression rhs) {
		this.lhs = lhs;
		this.rhs = rhs;
	}

	public Equal(String lhs, Expression rhs) {
		this.lhs = new Variable(lhs);
		this.rhs = rhs;
	}

	public Equal(Expression lhs, String rhs) {
		this.lhs = lhs;
		this.rhs = new Variable(rhs);
	}

	public Equal(String lhs, String rhs) {
		this.lhs = new Variable(lhs);
		this.rhs = new Variable(rhs);
	}
	
	public Object eval(Environment env) {
		
		
		Object lvalue = this.lhs.eval(env); //.toString());
		Object rvalue = this.rhs.eval(env); //.toString());
		
		if (lvalue == null && rvalue == null) {
			return true;
		} else if ((lvalue == null && rvalue != null) || (lvalue != null && rvalue == null)) {
			return false;
		} else if (lvalue instanceof Number && rvalue instanceof Number) {
			return ((Number) lvalue).equals((Number)rvalue);
		} else if (lvalue instanceof String && rvalue instanceof String) {
			return ((String)lvalue).equals((String)rvalue);
		} else {
			throw new RuntimeException("The passed arguments have types that are not supported (" + lvalue.getClass().getName() + "," + rvalue.getClass().getName() + ")");
		}
	}

	public String toString() {
		return this.lhs.toString() + " = " + this.rhs.toString();
	}
}
