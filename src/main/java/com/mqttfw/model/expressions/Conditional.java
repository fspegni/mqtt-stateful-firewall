package com.mqttfw.model.expressions;

import com.mqttfw.model.Environment;

public class Conditional extends Expression {

	Expression condition;
	Expression thenArgument;
	Expression elseArgument;
	
	public Conditional(Expression condition, Expression thenArgument, Expression elseArgument) {
		this.condition = condition;
		this.thenArgument = thenArgument;
		this.elseArgument = elseArgument;
	}
	
	public Object eval(Environment env) {
		Boolean c = (Boolean) condition.eval(env);
		
		Object result = c ? this.thenArgument.eval(env) : this.elseArgument.eval(env); 
		
		return result;
	}

	public String toString() {
		return this.condition.toString() + " ? " + this.thenArgument.toString() + " : " + this.elseArgument.toString();
	}
}
