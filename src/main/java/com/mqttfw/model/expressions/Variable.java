package com.mqttfw.model.expressions;

import com.mqttfw.model.Environment;

public class Variable extends Expression {

	String name;
	
	public Variable(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	public Object eval(Environment env) {
		return env.getValue(this.name);
	}
	
	public String toString() {
        return this.name;
    }

}
