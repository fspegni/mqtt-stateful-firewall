package com.mqttfw.model.expressions;

import com.mqttfw.model.Environment;
import org.json.JSONObject;
import org.json.JSONTokener;

public class JSONParse extends Expression {

    protected Expression arg;

    public JSONParse(Expression arg) {
        this.arg = arg;
    }

    public JSONParse(String varName) {
        this.arg = new Variable(varName);
    }

    @Override
    public Object eval(Environment env) {
        Object value = this.arg.eval(env);
        Object res = null;

        if (value instanceof String) {
            res = (JSONObject) new JSONTokener((String)value).nextValue();
        } else {
            throw new RuntimeException("Unexpected type for the passed argument. Expected String, received: " + value.getClass().getName());
        }

        if (res == null) {
            throw new RuntimeException("Returning a NULL JSONObject is not allowed. Argument: " + this.arg.toString());
        }
        return res;

    }

    public String toString() {
        return "JSONParse("  + this.arg.toString() + ")";
    }
    
}
