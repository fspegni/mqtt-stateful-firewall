package com.mqttfw.model.expressions;


import java.nio.charset.StandardCharsets;

import com.mqttfw.model.Environment;
import com.mqttfw.model.MQTTPacket;

public class ReadPayload extends Expression {

    protected Expression arg;

    public ReadPayload(String varName) {
        this.arg = new Variable(varName);
    }

	public Object eval(Environment env) {
        MQTTPacket packet = (MQTTPacket)this.arg.eval(env); // env.getWaitingQueue().element();

        byte[] b = packet.getPayload();
        String s;
        try {
            s = new String(b, StandardCharsets.UTF_8);
        } catch (Exception e) {
            return b;
        }

        try{
            if (s.equalsIgnoreCase("true")) {
                return true;
            } else if (s.equalsIgnoreCase("false")) {
                return false;
            } else {
                return Double.parseDouble(s);
            }
        }   catch (NumberFormatException e) {
            try {
                return Integer.parseInt(s);
            } catch (NumberFormatException e1) {
                return s;   
            }
        } 

    }

    public String toString() {
        return "ReadPayload("  + this.arg.toString() + ")";
    }

}