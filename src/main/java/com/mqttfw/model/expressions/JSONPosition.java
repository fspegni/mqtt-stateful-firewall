package com.mqttfw.model.expressions;

import com.mqttfw.model.Environment;

import org.json.JSONArray;

public class JSONPosition extends Expression {

    protected Expression arg;
    protected int position;

    public JSONPosition(Expression arg, int position) {
        this.arg = arg;
        this.position = position;
    }

    public JSONPosition(String varName, int position) {
        this.arg = new Variable(varName);
        this.position = position;
    }

    @Override
    public Object eval(Environment env) {
        Object value = this.arg.eval(env);

        if (! (value instanceof JSONArray)) {
            throw new RuntimeException("This expression applies only to object of type JSONArray. Received: " + value.getClass().getName());
        }

        return ((JSONArray)value).get(this.position);


    }

    public String toString() {
        return this.arg.toString() + "[" + String.valueOf(this.position) + "]";
    }
    
}
