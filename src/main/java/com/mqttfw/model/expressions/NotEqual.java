package com.mqttfw.model.expressions;

import com.mqttfw.model.Environment;

public class NotEqual extends Expression {

	protected Equal eq;
	
	public NotEqual(Expression lhs, Expression rhs) {
		this.eq = new Equal(lhs, rhs);
	}

	public NotEqual(String lhs, Expression rhs) {
		this.eq = new Equal(lhs, rhs);
	}
	
	public Object eval(Environment env) {
		
		return (Boolean)( this.eq.eval(env) ) != true;
	}

	public String toString() {
        return this.eq.lhs.toString() + " != " + this.eq.rhs.toString();
    }

}
