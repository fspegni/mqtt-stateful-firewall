package com.mqttfw.model.expressions;

import com.mqttfw.model.Environment;

public class Constant extends Expression {

	Object value;
	
	public Constant(Object value) {
		this.value = value;
	}
	
	public Object eval(Environment env) {
		return this.value;
	}
	
	public String toString() {
		return this.value.toString();
	}

}
