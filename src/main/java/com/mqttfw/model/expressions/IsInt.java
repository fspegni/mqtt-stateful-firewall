package com.mqttfw.model.expressions;

import com.mqttfw.model.Environment;
import com.mqttfw.model.MQTTPacket;
import org.fusesource.mqtt.codec.PUBLISH;

import java.nio.charset.StandardCharsets;

// TODO refactor this class to take an expression as argument
public class IsInt extends Expression {
    // protected int value;
    public IsInt(){

    }
    @Override
    public Object eval(Environment env) {
        MQTTPacket packet = env.getWaitingQueue().element();
        if(packet.messageType() != PUBLISH.TYPE){
            throw new RuntimeException("IsInt only works on publish packets");
        }
//        PUBLISH p = new PUBLISH();
        PUBLISH p = (PUBLISH)packet.msg;

        try {
//            p.decode(packet.msg.encode());

            byte[] b = p.payload().trim().toByteArray();
            String s = new String(b, StandardCharsets.UTF_8);
            Integer.parseInt(s);
            return true; //String is an Integer
        }   catch (NumberFormatException e) {
            return false; //String is not an Integer
        }


    }

}
