package com.mqttfw.model.expressions;

import com.mqttfw.model.Environment;

public abstract class Expression {
	public abstract Object eval(Environment env);

}
