package com.mqttfw.model.expressions;

import java.util.ArrayList;

import com.mqttfw.model.Environment;

public class Sum extends Expression {

	Expression[] arguments;
	
	public Sum(Expression ... arguments) {
		this.arguments = arguments;
	}
	
	public Object eval(Environment env) {
		
		Double result = 0.0; //Double.valueOf(0.0);
		
		for (Expression curr : this.arguments) {
			Double value = (Double) curr.eval(env);
			
			result = result + value;
		}
		
		return result;
	}

	public String toString() {
		ArrayList<String> parts = new ArrayList<String>();

		for (Expression e : this.arguments) {
			parts.add(e.toString());
		}
		return String.join(" + ", parts);
    }
}
