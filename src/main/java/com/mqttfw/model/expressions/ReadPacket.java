package com.mqttfw.model.expressions;


import java.util.Queue;

import com.mqttfw.model.Environment;
import com.mqttfw.model.MQTTPacket;

public class ReadPacket extends Expression {

    // protected int position = 0;

    public ReadPacket() {}

	public Object eval(Environment env) {
        Queue<MQTTPacket> q = env.getWaitingQueue();
        
        if (q.size() == 0) {
            throw new RuntimeException("The waiting queue is expected to be non-empty before attempting to read a packet");
        }
        MQTTPacket packet = q.element();

        if (packet == null) {
            throw new RuntimeException("Unexpected empty packet");
        }
        return packet;
        

    }

    public String toString() {
        return "ReadPacket()";
    }

}