package com.mqttfw.model.expressions;

import com.mqttfw.model.Environment;
import com.mqttfw.model.expressions.TimedObject.TimedObjectImplementation;

public class TimedObjectValue extends Expression {

    protected Expression  exp;

    public TimedObjectValue(String varName) {
        this.exp = new Variable(varName);
    }

    public TimedObjectValue(Expression exp) {
        this.exp = exp;
    }

    @Override
    public Object eval(Environment env) {
        try {
            TimedObjectImplementation to = (TimedObjectImplementation)this.exp.eval(env);

            return to.getValue();
        } catch (ClassCastException e) {
            throw new RuntimeException("Timer does not work because the specified expression (" + this.exp + ") does not point to a TimedObject");
        }
    }
    
    public String toString() {
        return "TimedObjectValue(" + this.exp.toString() + ")";
    }
}
