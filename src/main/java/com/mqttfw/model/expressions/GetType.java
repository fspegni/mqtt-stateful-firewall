package com.mqttfw.model.expressions;

import com.mqttfw.model.Environment;

public class GetType extends Expression {
    protected int value;

    protected Expression arg;

    protected Variable var;

    public GetType(String varName) {
        this.var = new Variable(varName);
    }

    public GetType(Variable var) {
        this.var = var;
    }

    public GetType(Expression arg){
        this.arg = arg;
    }

    @Override
    public Object eval(Environment env) {

        Object arg = this.arg;
        if (arg == null) {
            arg = this.var.eval(env);
        }

        if (arg == null) {
            throw new RuntimeException("Unable to retrieve value of argument or variable");
        }

        return arg.getClass().getName();
    }

    public String toString() {
        return "GetType("  + this.arg.toString() + ")";
    }
}
