package com.mqttfw.model.expressions;

import java.time.Clock;

import com.mqttfw.model.Environment;

public class ClockVariable extends Variable {

	String name;
	long reset;
	static Clock c = Clock.systemDefaultZone();
	
	public ClockVariable(String name) {
		super(name);
		this.reset = c.millis(); 
	}
	
	
	public Object eval(Environment env) {
		long reset = (long)env.getValue(this.name);
		if (reset <= 0) {
			reset = this.reset;
		}

		return c.millis() - reset;
	}
	
	public String toString() {
		return this.name;
	}

}
