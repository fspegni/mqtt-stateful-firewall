package com.mqttfw.model.expressions;

import java.util.Collection;

import com.mqttfw.model.Environment;

public class Len extends Expression {
    
    Expression arg;

    public Len(String varName) {
        this.arg = new Variable(varName);
    }

    public Len(Expression arg) {
        this.arg = arg;
    }

    public Object eval(Environment env) {
        try {
            Collection<?> c = (Collection<?>)this.arg.eval(env);
            return c.size();

        } catch (ClassCastException e) {
            throw new RuntimeException("Len only applies to expressions returning a Collection (or a subtype of it)");
        }
    }

    public String toString() {
        return "Len("  + this.arg.toString() + ")";
    }
}
