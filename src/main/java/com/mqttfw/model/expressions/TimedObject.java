package com.mqttfw.model.expressions;

import java.time.Clock;

import com.mqttfw.model.Environment;

public class TimedObject extends Expression {

    public class TimedObjectImplementation {

        protected Object value;
        protected long timestamp;

        public TimedObjectImplementation(Object value) {
            this.value = value;
            this.timestamp = Clock.systemDefaultZone().millis();
        }

        public long getTimestamp() {
            return this.timestamp;
        }

        public long getTimerValue() {
            return Clock.systemDefaultZone().millis() - this.timestamp;
        }

        public Object getValue() {
            return this.value;
        }

        public String toString() {
            return "("  + this.value.toString() + ", " + String.valueOf(this.getTimerValue()) + ")";
        }
    }

    Expression arg;

    public TimedObject(String varName) {
        this.arg = new Variable(varName);
    }
    
    public TimedObject(Expression arg) {
        this.arg = arg;
    }

    @Override
    public Object eval(Environment env) {
        Object value = this.arg.eval(env);

        return new TimedObjectImplementation(value);
    }
    
    public String toString() {
        return "TimedObject("  + this.arg.toString() + ")";
    }
}
