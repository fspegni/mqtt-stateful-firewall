package com.mqttfw.model.expressions;

import java.util.Collection;

import com.mqttfw.model.Environment;

public class IsEmpty extends Expression {
    
    Expression arg;

    public IsEmpty(String varName) {
        this.arg = new Variable(varName);
    }

    public IsEmpty(Expression arg) {
        this.arg = arg;
    }

    public Object eval(Environment env) {
        try {
            Collection<?> c = (Collection<?>)this.arg.eval(env);
            return c.size() == 0;

        } catch (ClassCastException e) {
            throw new RuntimeException("IsEmpty only applies to expressions returning a Collection (or a subtype of it)");
        }
    }

    public String toString() {
        return "IsEmpty("  + this.arg.toString() + ")";
    }
}
