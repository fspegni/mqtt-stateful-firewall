package com.mqttfw.model.expressions;

import com.mqttfw.model.Environment;

public class Negate extends Expression {

    Expression arg;

    public Negate(Expression exp) {
        this.arg = exp;
    }

    @Override
    public Object eval(Environment env) {
        return ! (Boolean)(this.arg.eval(env));
    }

    public String toString() {
        return "! "  + this.arg.toString();
    }

}