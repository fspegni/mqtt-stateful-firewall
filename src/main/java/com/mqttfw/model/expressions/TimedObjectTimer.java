package com.mqttfw.model.expressions;

import com.mqttfw.model.Environment;
import com.mqttfw.model.expressions.TimedObject.TimedObjectImplementation;

public class TimedObjectTimer extends Expression {

    protected Expression exp;

    public TimedObjectTimer(String varName) {
        this.exp = new Variable(varName);
    }

    public TimedObjectTimer(Expression exp) {
        this.exp = exp;
    }
    
    @Override
    public Object eval(Environment env) {
        try {
            TimedObjectImplementation to = (TimedObjectImplementation)this.exp.eval(env);

            return to.getTimerValue();
        } catch (ClassCastException e) {
            throw new RuntimeException("Timer does not work because the specified expression (" + this.exp + ") does not point to a TimedObject");
        }
    }
    
    public String toString() {
        return "TimedObjectTimer(" + this.exp.toString() + ")";
    }
}
