package com.mqttfw.model.expressions;

import java.util.ArrayList;

import com.mqttfw.model.Environment;

public class Diff extends Expression {

	Expression[] arguments;
	
	public Diff(Expression ...arguments) {
		this.arguments = arguments;
	}
	
	public Object eval(Environment env) {
		
		Double result = null;
		
		for (Expression curr : this.arguments) {
			
			Number value = (Number)curr.eval(env);

			if (env.isVerbose()) {
				System.out.println("Diff argument: " + curr.toString() + " |-> " + value);
			}

			if (value == null) {
				throw new RuntimeException("The following expression evaluates to null: " + curr.toString());
			}
			// TODO check the following
			result = result == null ? value.doubleValue() : result - value.doubleValue(); // first time, take the first argument value, next subtract
		}
		
		return result;
	}

	public String toString() {
		ArrayList<String> parts = new ArrayList<>();
        
        for (Expression e : this.arguments) {
            parts.add(e.toString());
        }
        return String.join(" - ", parts);
	}
}
