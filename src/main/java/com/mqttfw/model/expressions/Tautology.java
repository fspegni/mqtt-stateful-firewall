package com.mqttfw.model.expressions;

import com.mqttfw.model.Environment;

public class Tautology extends Expression {

	public Object eval(Environment env) {
		return Boolean.TRUE;
	}

	public String toString() {
        return "1 = 1";
    }

}
