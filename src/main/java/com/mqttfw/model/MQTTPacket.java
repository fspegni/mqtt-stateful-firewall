package com.mqttfw.model;

import org.fusesource.hawtbuf.Buffer;
import org.fusesource.mqtt.codec.*;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.net.ProtocolException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class MQTTPacket  {

	public static class RemainingLength {
		int size;
		int value;

		public RemainingLength(int size, int value) {
			this.size = size;
			this.value = value;
		}

		public int getSize() {
			return this.size;
		}

		public int getValue() {
			return this.value;
		}
	}

	public MessageSupport.Message msg;
	
	static Map<Byte,Class> type2Message = new HashMap<Byte, Class>();
	
	static {
		type2Message.put(CONNACK.TYPE, CONNACK.class);
		type2Message.put(CONNECT.TYPE, CONNECT.class);
		type2Message.put(DISCONNECT.TYPE, DISCONNECT.class);
		type2Message.put(PINGREQ.TYPE, PINGREQ.class);
		type2Message.put(PINGRESP.TYPE, PINGRESP.class);
		type2Message.put(PUBACK.TYPE, PUBACK.class);
		type2Message.put(PUBCOMP.TYPE, PUBCOMP.class);
		type2Message.put(PUBLISH.TYPE, PUBLISH.class);
		type2Message.put(PUBREC.TYPE, PUBREC.class);
		type2Message.put(PUBREL.TYPE, PUBREL.class);
		type2Message.put(SUBACK.TYPE, SUBACK.class);
		type2Message.put(SUBSCRIBE.TYPE, SUBSCRIBE.class);
		type2Message.put(UNSUBACK.TYPE, UNSUBACK.class);
		type2Message.put(UNSUBSCRIBE.TYPE, UNSUBSCRIBE.class);	
	}
	
	public MQTTPacket(byte type) {
		try {
			initMessage(type);
		} catch (Exception e) {
			throw new IllegalArgumentException("The type '" + type + "' generated the following error: " + e.getMessage());
		}
	}

	public String toString() {
		try {
			String payload = new String(this.getPayload());
			if (payload.length() > 23) {
				payload = payload.substring(0, 20) + "...";
			}
			return super.toString() + " : " + payload;
		} catch (RuntimeException e) {
			return super.toString();
		}
		/* 
		if (this.msg.getClass() == PUBLISH.class) {
			String payload = new String(((PUBLISH)this.msg).payload().toByteArray());
			if (payload.length() > 23) {
				payload = payload.substring(0, 20) + "...";
			}
			return super.toString() + " : " + payload; 
		} else {
			return super.toString();
		} */
	}

	public byte[] getPayload() {
		
		if (this.msg.getClass() != PUBLISH.class) {
			throw new RuntimeException("Only packets of type PUBLISH have a payload");
		}

		return ((PUBLISH)this.msg).payload().toByteArray();
		
	}

	public JSONObject getJSON() {
		String payload = new String(this.getPayload());
		return (JSONObject) new JSONTokener(payload).nextValue();
	}

	private void buildPacket(MQTTFrame f, Buffer b) throws ProtocolException {
		byte type = f.messageType();
		try { 
			Class klass = type2Message.get(type);
			this.msg = (MessageSupport.Message)(klass.getConstructor().newInstance());
			this.msg.decode(f);
			
			
		} catch (Exception e) {
			System.err.println("Error handling packet of type: " + type);
			e.printStackTrace(System.err);
			throw new ProtocolException("Message type '" + type + "' is not handled correctly at the moment");
			
		}

	}

	public MQTTPacket(byte[] data) throws ProtocolException {

		RemainingLength rl = MQTTPacket.decodeRemainingLength(data, 1);

		// skip the remaining length bytes, and consider the following bytes as part of the MQTT frame buffer
		Buffer b = new Buffer(data, 1 + rl.size, rl.value);
		MQTTFrame f = new MQTTFrame(b);

		// the frame header is always the first byte
		f.header(data[0]);

        buildPacket(f, b);
	}
	
	
	public MQTTPacket(byte header, byte[] data) throws ProtocolException {


		Buffer b = new Buffer(data);
		MQTTFrame f = new MQTTFrame(b);
		f.header(header);

		buildPacket(f, b);

	}
	
	
	private void initMessage(byte type) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		Class klass = type2Message.get(type);
		this.msg = (MessageSupport.Message)(klass.getConstructor().newInstance());

	}
	
	public byte[] serialize() throws UnsupportedEncodingException {
		
		MQTTFrame f = this.msg.encode();
		int fSize = this.getFrameSize(f);
		byte[] rl = this.encodeRemainingLength(fSize);

		int bufferSize = 1 + rl.length + fSize;
        int pos = 0;

		byte[] buffer = new byte[bufferSize]; 

		buffer[pos++] = f.header();
		System.arraycopy(rl, 0, buffer, pos, rl.length); // copy the remaining length onto the output buffer
		pos += rl.length;

		for (Buffer b : f.buffers) {
			System.arraycopy(b.data, b.offset, buffer, pos, b.length);
			pos += b.length;
		}

		return buffer;
	}
	

	private int getFrameSize(MQTTFrame f) {
		int size = 0;
		for (Buffer b : f.buffers) {
			size += b.getLength();
		}

		return size;
	}

	public byte messageType() {
		return this.msg.messageType();
	}

	public byte[] encodeRemainingLength(int value) {
		// TESTED
		byte[] buffer = new byte[4];
		int pos = 0;
		do {
			byte encoded = (byte)(value % 128);
			value = value / 128;
			if (value > 0) {
				encoded = (byte)(encoded | 128);
			}

			buffer[pos++] = encoded;
		} while (value > 0);

		return Arrays.copyOfRange(buffer, 0, pos);
	}

	public static RemainingLength decodeRemainingLength(MQTTFrame frame) {
		// TESTED
		if (frame.buffers.length == 0) {
			throw new RuntimeException("Cannot decode remaining length of an empty MQTT frame");
		}

		Buffer b = frame.buffers[0];
		return MQTTPacket.decodeRemainingLength(b.data, b.offset);
	}

	public static RemainingLength decodeRemainingLength(byte[] buffer, int pos) {
		// TESTED
		int size = 0;
		int multiplier = 1;
		int value = 0;
		byte encoded;
		do {
			encoded = buffer[pos+size];
			value += (encoded & 127) * multiplier;
			multiplier *= 128;
			if (multiplier > 128 * 128 * 128) {
				throw new RuntimeException("Malformed remaining lenght");
			}
			size++;

		} while((encoded & 128) != 0);

		return new RemainingLength(size, value);
	}
}
