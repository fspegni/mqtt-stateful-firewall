package com.mqttfw.integration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.ProtocolException;
import java.util.ArrayList;
import java.util.List;
import com.mqttfw.model.Environment;
import com.mqttfw.model.expressions.*;
import com.mqttfw.model.expressions.Variable;
import com.mqttfw.model.statements.*;
import org.fusesource.hawtbuf.Buffer;
import org.fusesource.hawtbuf.UTF8Buffer;
import org.fusesource.mqtt.client.QoS;
import org.fusesource.mqtt.client.Topic;
import org.fusesource.mqtt.codec.PUBLISH;
import org.fusesource.mqtt.codec.SUBSCRIBE;
import org.junit.Test;
import com.mqttfw.filter.Firewall;
import com.mqttfw.model.MQTTPacket;
import com.mqttfw.model.Rule;

public class TestIntegration {

	@Test
	public void testBasicIntegration() {
		Firewall f = new Firewall();

		assertNotEquals(null, f.getEnvironment());
		assertNotEquals(null, f.getInputStream());
		assertNotEquals(null, f.getOutputStream());
		assertEquals(0, f.getRules().size());

		Rule r = new Rule(new Tautology(), new CopyCat());
		f.addRule(r);

		assertEquals(1, f.getRules().size());

		assertTrue(r.isEnabled(f.getEnvironment()));

		// forge new packet for test
		PUBLISH p = new PUBLISH();
		p.payload(new Buffer("hello world".getBytes()));
		p.topicName(new UTF8Buffer("foo_topic"));

		byte header = p.encode().header();
		byte[] bufferWrite = p.encode().buffers[0].data;

		try {
			MQTTPacket packet = new MQTTPacket(header, bufferWrite);

			f.write(packet.serialize());

			byte[] bufferRead = new byte[packet.serialize().length];
			int nread = f.read(bufferRead);

			// +1 perche bufferWrite non ha header
			assertEquals(nread, bufferWrite.length + 1);

			assertEquals(bufferRead[0], header);

			for (int i = 0; i < bufferWrite.length; i++) {
				assertEquals(bufferWrite[i], bufferRead[1 + i]);
			}

		} catch (ProtocolException e) {
			e.printStackTrace();
			fail("Unexpected exception: " + e.getMessage());
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			fail("Unexpected exception: " + e.getMessage());

		} catch (IOException e) {
			e.printStackTrace();
			fail("Unexpected exception: " + e.getMessage());

		}
	}


	@Test
	public void testPacketTypeFiltering() {
		Firewall f = new Firewall();

		assertNotEquals(null, f.getEnvironment());
		assertNotEquals(null, f.getInputStream());
		assertNotEquals(null, f.getOutputStream());
		assertEquals(0, f.getRules().size());


		byte typeToCheck = PUBLISH.TYPE; 

		Rule r = new Rule(new PacketHasType(typeToCheck), new CopyCat());
		f.addRule(r);

		assertEquals(1, f.getRules().size());

		// questo assert non si puo mettere qui
		//assertTrue(r.isEnabled(f.getEnvironment()));

		// forge some packets for test
		// first packet
		PUBLISH p1 = new PUBLISH();
		p1.payload(new Buffer("hello world from packet 1".getBytes()));
		p1.topicName(new UTF8Buffer("foo_topic"));

		byte header1 = p1.encode().header();
		byte[] bufferWrite1 = p1.encode().buffers[0].data;

		// second packet
		SUBSCRIBE p2 = new SUBSCRIBE();
		Topic[] topic = {new Topic("foo_topic", QoS.AT_LEAST_ONCE)};
		p2.topics(topic);
		byte header2 = p2.encode().header();
		byte[] bufferWrite2 = p2.encode().buffers[0].data;

		// third packet
		PUBLISH p3 = new PUBLISH();
		p3.payload(new Buffer("hello world from packet 3".getBytes()));
		p3.topicName(new UTF8Buffer("foo_topic"));
		byte header3 = p3.encode().header();
		byte[] bufferWrite3 = p3.encode().buffers[0].data;

		// fourth packet
		SUBSCRIBE p4 = new SUBSCRIBE();
		Topic[] topic4 = {new Topic("foo_topic", QoS.AT_LEAST_ONCE)};
		p2.topics(topic4);
		byte header4 = p4.encode().header();
		byte[] bufferWrite4 = p4.encode().buffers[0].data;

		try {
			List<MQTTPacket> packetList = new ArrayList<MQTTPacket>();
			MQTTPacket packet1 = new MQTTPacket(header1, bufferWrite1);
			MQTTPacket packet2 = new MQTTPacket(header2, bufferWrite2);
			MQTTPacket packet3 = new MQTTPacket(header3, bufferWrite3);
			MQTTPacket packet4 = new MQTTPacket(header4, bufferWrite4);

			packetList.add(packet1);
			packetList.add(packet2);
			packetList.add(packet3);
			packetList.add(packet4);

			for (int i = 0; i < packetList.size(); i++) {
				f.write(packetList.get(i).serialize());
			}


			byte[] bufferRead1 = new byte[bufferWrite1.length + 1];
			int nread1 = f.read(bufferRead1);
			assertEquals(nread1, bufferWrite1.length + 1);
			assertEquals(bufferRead1[0], header1);

			for (int i = 0; i < bufferWrite1.length; i++) {
				assertEquals(bufferWrite1[i], bufferRead1[1 + i]);
			}

			byte[] bufferRead3 = new byte[bufferWrite3.length + 1];
			int nread3 = f.read(bufferRead3);
			assertEquals(nread3, bufferWrite3.length + 1);
			assertEquals(bufferRead3[0], header3);

			for (int i = 0; i < bufferWrite3.length; i++) {
				assertEquals(bufferWrite3[i], bufferRead3[1 + i]);
			}

		} catch (ProtocolException e) {
			e.printStackTrace();
			fail("Unexpected exception: " + e.getMessage());
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			fail("Unexpected exception: " + e.getMessage());

		} catch (IOException e) {
			e.printStackTrace();
			fail("Unexpected exception: " + e.getMessage());

		}
	}


	@Test
	public void testCountPackets() {
		Firewall f = new Firewall();

		assertNotEquals(null, f.getEnvironment());
		assertNotEquals(null, f.getInputStream());
		assertNotEquals(null, f.getOutputStream());
		assertEquals(0, f.getRules().size());

		f.getEnvironment().addVariable("N_PACKETS");
		f.getEnvironment().setValue("N_PACKETS", 0);

		Rule r1 = new Rule(new PacketHasType(PUBLISH.TYPE),
				new Increment(new Variable("N_PACKETS")));
		f.addRule(r1);

		Rule r2 = new Rule(new PacketHasType(PUBLISH.TYPE),
				new IfThenElse(new GTE(new Variable("N_PACKETS"), new Constant(3)),
						new CopyCat(), new Ignore()));
		f.addRule(r2);

		assertEquals(2, f.getRules().size());

		try {
			for (int i = 0; i < 10; i++) {
				// forge new packet for test
				PUBLISH p = new PUBLISH();
				p.payload(new Buffer(("hello world " + i).getBytes()));
				p.topicName(new UTF8Buffer("foo_topic"));

				byte header = p.encode().header();
				byte[] bufferWrite = p.encode().buffers[0].data;


				MQTTPacket packet = new MQTTPacket(header, bufferWrite);

				f.write(packet.serialize());

			}

			assertEquals(Double.valueOf(10), Double.valueOf(f.getEnvironment().getValue("N_PACKETS").toString()));
			assertEquals(f.getEnvironment().getWaitingQueue().size(), 0);
			assertEquals(8, f.getEnvironment().getFilteredQueue().size());

		} catch (ProtocolException e) {
			e.printStackTrace();
			fail("Unexpected exception: " + e.getMessage());
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			fail("Unexpected exception: " + e.getMessage());

		} catch (IOException e) {
			e.printStackTrace();
			fail("Unexpected exception: " + e.getMessage());

		}
	}

	@Test
	public void testRejectedPackets() {
		Firewall f = new Firewall();

		assertNotEquals(null, f.getEnvironment());
		assertNotEquals(null, f.getInputStream());
		assertNotEquals(null, f.getOutputStream());
		assertEquals(0, f.getRules().size());
		Environment env = f.getEnvironment();
		env.addVariable("_QUEUE_REJECTED");
		env.setValue("_QUEUE_REJECTED", new ArrayList<MQTTPacket>());

		byte typeToCheck = PUBLISH.TYPE; 
		Rule r1 = new Rule(new PacketHasType(typeToCheck), new CopyCat());
		Rule r2 = new Rule(new Tautology(), new CopyCat("_QUEUE_REJECTED"));


		f.addRule(r1);
		f.addRule(r2);


		assertEquals(2, f.getRules().size());

		// questo assert non si puo mettere qui
		//assertTrue(r.isEnabled(f.getEnvironment()));

		// forge some packets for test
		// first packet
		PUBLISH p1 = new PUBLISH();
		p1.payload(new Buffer("hello world from packet 1".getBytes()));
		p1.topicName(new UTF8Buffer("foo_topic"));

		byte header1 = p1.encode().header();
		byte[] bufferWrite1 = p1.encode().buffers[0].data;

		// second packet
		SUBSCRIBE p2 = new SUBSCRIBE();
		Topic[] topic = {new Topic("foo_topic", QoS.AT_LEAST_ONCE)};
		p2.topics(topic);
		byte header2 = p2.encode().header();
		byte[] bufferWrite2 = p2.encode().buffers[0].data;

		// third packet
		PUBLISH p3 = new PUBLISH();
		p3.payload(new Buffer("hello world from packet 3".getBytes()));
		p3.topicName(new UTF8Buffer("foo_topic"));
		byte header3 = p3.encode().header();
		byte[] bufferWrite3 = p3.encode().buffers[0].data;

		// fourth packet
		SUBSCRIBE p4 = new SUBSCRIBE();
		Topic[] topic4 = {new Topic("foo_topic", QoS.AT_LEAST_ONCE)};
		p4.topics(topic4);
		byte header4 = p4.encode().header();
		byte[] bufferWrite4 = p4.encode().buffers[0].data;

		try {
			List<MQTTPacket> packetList = new ArrayList<MQTTPacket>();
			MQTTPacket packet1 = new MQTTPacket(header1, bufferWrite1);
			MQTTPacket packet2 = new MQTTPacket(header2, bufferWrite2);
			MQTTPacket packet3 = new MQTTPacket(header3, bufferWrite3);
			MQTTPacket packet4 = new MQTTPacket(header4, bufferWrite4);

			packetList.add(packet1);
			packetList.add(packet2);
			packetList.add(packet3);
			packetList.add(packet4);

			for (int i = 0; i < packetList.size(); i++) {
				f.write(packetList.get(i).serialize());
			}

			byte[] bufferRead1 = new byte[bufferWrite1.length + 1];
			int nread1 = f.read(bufferRead1);
			assertEquals(nread1, bufferWrite1.length + 1);
			assertEquals(bufferRead1[0], header1);

			for (int i = 0; i < bufferWrite1.length; i++) {
				assertEquals(bufferWrite1[i], bufferRead1[1 + i]);
			}

			byte[] bufferRead3 = new byte[bufferWrite3.length + 1];
			int nread3 = f.read(bufferRead3);
			assertEquals(nread3, bufferWrite3.length + 1);
			assertEquals(bufferRead3[0], header3);

			for (int i = 0; i < bufferWrite3.length; i++) {
				assertEquals(bufferWrite3[i], bufferRead3[1 + i]);
			}

		} catch (ProtocolException e) {
			e.printStackTrace();
			fail("Unexpected exception: " + e.getMessage());
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			fail("Unexpected exception: " + e.getMessage());

		} catch (IOException e) {
			e.printStackTrace();
			fail("Unexpected exception: " + e.getMessage());

		}

	}

	@Test

	public void TestSumPayloads() {
		Firewall f = new Firewall();

		assertNotEquals(null, f.getEnvironment());
		assertNotEquals(null, f.getInputStream());
		assertNotEquals(null, f.getOutputStream());
		assertEquals(0, f.getRules().size());

		Environment env = f.getEnvironment();
		env.addVariable("_PAYLOADS_SUM");
		env.addVariable("_QUEUE_REJECTED");
		env.setValue("_QUEUE_REJECTED", new ArrayList<MQTTPacket>());
		env.setValue("_PAYLOADS_SUM", 0);
		byte typeToCheck = PUBLISH.TYPE;


		Rule r1 = new Rule(new PacketHasType(typeToCheck), new IfThenElse(new IsInt(), new IncrementPayload(new Variable("_PAYLOADS_SUM"))));
		Rule r2 = new Rule(new PacketHasType(typeToCheck), new CopyCat(), true);
		Rule r3 = new Rule(new Tautology(), new CopyCat("_QUEUE_REJECTED"));

		f.addRule(r1);
		f.addRule(r2);
		f.addRule(r3);

		assertEquals(3, f.getRules().size());

		// forge some packets for test
		// first packet
		PUBLISH p1 = new PUBLISH();
		p1.payload(new UTF8Buffer("12"));
		p1.topicName(new UTF8Buffer("foo_topic"));

		byte header1 = p1.encode().header();
		byte[] bufferWrite1 = p1.encode().buffers[0].data;

		// second packet
		PUBLISH p2 = new PUBLISH();
		p2.payload(new Buffer("2".getBytes()));
		p2.topicName(new UTF8Buffer("foo_topic"));

		byte header2 = p2.encode().header();
		byte[] bufferWrite2 = p2.encode().buffers[0].data;

		// third packet
		PUBLISH p3 = new PUBLISH();
		p3.payload(new Buffer("3".getBytes()));
		p3.topicName(new UTF8Buffer("foo_topic"));
		byte header3 = p3.encode().header();
		byte[] bufferWrite3 = p3.encode().buffers[0].data;

		// fourth packet
		PUBLISH p4 = new PUBLISH();
		p4.payload(new Buffer("4".getBytes()));
		p4.topicName(new UTF8Buffer("foo_topic"));

		byte header4 = p4.encode().header();
		byte[] bufferWrite4 = p4.encode().buffers[0].data;

		try {
			List<MQTTPacket> packetList = new ArrayList<MQTTPacket>();
			MQTTPacket packet1 = new MQTTPacket(header1, bufferWrite1);
			MQTTPacket packet2 = new MQTTPacket(header2, bufferWrite2);
			MQTTPacket packet3 = new MQTTPacket(header3, bufferWrite3);
			MQTTPacket packet4 = new MQTTPacket(header4, bufferWrite4);

			packetList.add(packet1);
			packetList.add(packet2);
			packetList.add(packet3);
			packetList.add(packet4);

			for (int i = 0; i < packetList.size(); i++) {
				f.write(packetList.get(i).serialize());
			}

			assertEquals(21F, f.getEnvironment().configuration.get("_PAYLOADS_SUM"));

		}  catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void TestTopic(){
		Firewall f = new Firewall();

		assertNotEquals(null, f.getEnvironment());
		assertNotEquals(null, f.getInputStream());
		assertNotEquals(null, f.getOutputStream());
		assertEquals(0, f.getRules().size());

		Environment env = f.getEnvironment();
		env.addVariable("_PAYLOADS_SUM");
		env.addVariable("_QUEUE_REJECTED");
		env.setValue("_PAYLOADS_SUM", 0);
		byte typeToCheck = PUBLISH.TYPE; 

		Rule r = new Rule(new PacketHasType(typeToCheck), new IfThenElse(new IsInt(),new Sequence(new IncrementPayload(new Variable("_PAYLOADS_SUM")),new CopyCat()),new Sequence(new Assignment(new Variable("_QUEUE_REJECTED"),new Constant(new ArrayList<MQTTPacket>())),new CopyCat("_QUEUE_REJECTED"))));
		f.addRule(r);
		assertEquals(1, f.getRules().size());

		// forge some packets for test
		// first packet
		PUBLISH p1 = new PUBLISH();
		p1.payload(new Buffer("1".getBytes()));
		p1.topicName(new UTF8Buffer("foo_topic"));

	}

}

