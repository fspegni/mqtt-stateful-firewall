package com.mqttfw.model;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.ProtocolException;
import java.util.ArrayDeque;
import java.util.Queue;

import org.fusesource.hawtbuf.Buffer;
import org.fusesource.hawtbuf.UTF8Buffer;
import org.fusesource.mqtt.codec.CONNACK;
import org.fusesource.mqtt.codec.PUBLISH;
import org.junit.Test;

 

public class TestMQTTPacketInputStream {

	@Test
	public void testNonBlockingStreamWithEmptyQueue() {
		Queue<MQTTPacket> q = new ArrayDeque<MQTTPacket>();
		
		assertTrue(q.isEmpty());
		
		// instantiate non blocking
		try (MQTTPacketInputStream is = new MQTTPacketInputStream(q, null)) {
			is.read();
			fail("Should not be allowed to call read() in non-blocking mode");
		} catch (IOException e) {
			assertTrue("Only IOException's due to IllegalStateException's are allowed here", e.getCause() instanceof IllegalStateException);
		}
		
	}
/*
	@Test(timeout=5000)
	public void testBlockingStreamWithEmptyQueue() throws InterruptedException {
		Queue<MQTTPacket> q = new ArrayDeque<MQTTPacket>();
		
		assertTrue(q.isEmpty());
		
		// instantiate non blocking
		MQTTPacketInpuStream is = new MQTTPacketInpuStream(q, true);
		
		try {
			is.read();

			fail("You should not reach this point");
		} catch (IOException e) {
			assertTrue("Only InterruptedException due to test timeout are expected here", e.getCause() instanceof InterruptedException);
			
		} 
	}	
*/	
	@Test(expected = IllegalArgumentException.class)
	public void testCannotInstantiateWithoutQueue() {
		
		try (MQTTPacketInputStream is = new MQTTPacketInputStream(null)) {

		} catch (IOException e) {
			fail("Should not reach this point");
		}
	}
	
	@Test(timeout=5000)
	public void testCanInstantiateWithNonEmptyQueue() {

		// preparation of the mqtt packet
		Queue<MQTTPacket> q = new ArrayDeque<MQTTPacket>();
		
		
		PUBLISH p = new PUBLISH();
		p.payload(new Buffer("hello world".getBytes()));
		p.topicName(new UTF8Buffer("foo_topic"));

		MQTTPacket packet;

		try {
			byte header = p.encode().header();
			byte[] input = p.encode().buffers[0].data;
			packet = new MQTTPacket(header, input);
			q.add(packet);
			
			assertFalse(q.isEmpty());
			
			// initiate the input stream
			try (MQTTPacketInputStream is = new MQTTPacketInputStream(q)) {
			
				byte[] content = new byte[input.length+1];
				int nread = is.read(content);
				
				assertEquals(input.length+1, nread);
				
				assertEquals(header, content[0]);
				
				for (int i=0; i<input.length; i++) {
					assertEquals(input[i], content[1+i]);
				}
			}

		} catch (ProtocolException e) {
			fail(e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			fail(e.getMessage());
			e.printStackTrace();
		} 

		
	}

	@Test //(timeout=5000)
	public void testBlockingStreamWithNonEmptyQueue() throws InterruptedException {
		Queue<MQTTPacket> q = new ArrayDeque<MQTTPacket>();
		
		// add 1st packet to the queue
		MQTTPacket p1 = new MQTTPacket(CONNACK.TYPE);
		q.add(p1);
		
		// add 2nd packet to the queue
		PUBLISH p2aux = new PUBLISH();

		p2aux.payload(new Buffer("hello world".getBytes()));
		p2aux.topicName(new UTF8Buffer("foo_topic"));
		
		MQTTPacket p2;
		try {
			p2 = new MQTTPacket(p2aux.encode().header(), p2aux.encode().buffers[0].data);

			q.add(p2);
			

			assertFalse(q.isEmpty());
			
			
			byte[] expected1 = p1.serialize();
			byte[] expected2 = p2.serialize();
			
			// instantiate non blocking
			try (MQTTPacketInputStream is = new MQTTPacketInputStream(q, true)) {
				for (int i=0; i<expected1.length; i++) {
					int byte_data = is.read();
					assertEquals(expected1[i], byte_data);
				}
				
				for (int i=0; i<expected2.length; i++) {
					int byte_data = is.read();
					assertEquals(expected2[i], byte_data);
				}
			}

			
		} catch (ProtocolException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			fail("Unexpected error: " + e1.getMessage());
			
		} catch (IOException e) {
			fail("Unexpected error: " + e.getMessage());
		} 

		
	}
		
	
}
