package com.mqttfw.model;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.ProtocolException;
import java.util.ArrayDeque;
import java.util.Queue;

import org.fusesource.hawtbuf.Buffer;
import org.fusesource.hawtbuf.UTF8Buffer;
import org.fusesource.mqtt.codec.PUBLISH;
import org.junit.Test;

public class TestMQTTPacketOutputStream {

	@Test
	public void testCanInstantiateWithEmptyQueue() {

		Queue<MQTTPacket> q = new ArrayDeque<MQTTPacket>();
		
		assertTrue(q.isEmpty());
		
		try (MQTTPacketOutputStream os = new MQTTPacketOutputStream(q)) {
			assertTrue(os.getQueue().isEmpty());
			assertEquals(os.getQueue(), q);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testCannotInstantiateWithoutQueue() {
		
		try (MQTTPacketOutputStream os = new MQTTPacketOutputStream(null)) {

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testCanInstantiateWithNonEmptyQueue() {

		// preparation of the mqtt packet
		Queue<MQTTPacket> q = new ArrayDeque<MQTTPacket>();
		
		
		PUBLISH p = new PUBLISH();
		p.payload(new Buffer("hello world".getBytes()));
		p.topicName(new UTF8Buffer("foo_topic"));
		

		MQTTPacket packet;
		try {
			byte header = p.encode().header();
			byte[] input = p.encode().buffers[0].data;
			packet = new MQTTPacket(header, input);
			q.add(packet);
			
			assertFalse(q.isEmpty());
			
			// initiate the input stream
			try (MQTTPacketOutputStream os = new MQTTPacketOutputStream(q)) {
			
				assertFalse(os.getQueue().isEmpty());
	
			} 

		} catch (ProtocolException e) {
			fail(e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			fail(e.getMessage());
			e.printStackTrace();
		}

		
	}

}
