package com.mqttfw.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.UnsupportedEncodingException;
import java.net.ProtocolException;
import org.fusesource.hawtbuf.Buffer;
import org.fusesource.hawtbuf.UTF8Buffer;
import org.fusesource.mqtt.client.QoS;
import org.fusesource.mqtt.codec.CONNACK;
import org.fusesource.mqtt.codec.PUBLISH;
import org.junit.Test;

import junit.framework.Assert;

public class TestMQTTTPacket {
	@Test
	public void canInstantiatePacketWithArguments() {
		MQTTPacket p = new MQTTPacket(CONNACK.TYPE);
		
		assertEquals(CONNACK.TYPE, p.messageType());
	}
	
	@Test
	public void canInstantiatePacketWithByteStream() {
		
		try {
			PUBLISH p = new PUBLISH();
			p.payload(new Buffer("hello world".getBytes()));
			p.topicName(new UTF8Buffer("foo_topic"));
			p.dup(true);
			p.retain(false);
			p.qos(QoS.AT_LEAST_ONCE);
			
			byte header = p.encode().header();
			byte[] buffer = p.encode().buffers[0].data;
			
			MQTTPacket packet = new MQTTPacket(header, buffer);
			
			// TODO i test sotto ancora falliscono perche` non e` stato implementato il parsing del byte stream
			assertEquals(PUBLISH.TYPE, packet.msg.messageType());
			assertEquals(PUBLISH.class, packet.msg.getClass());
			
			assertEquals(p.dup(), ((PUBLISH)packet.msg).dup());
			assertEquals(p.retain(), ((PUBLISH)packet.msg).retain());
			assertEquals(p.qos(), ((PUBLISH)packet.msg).qos());
			
			 
		} catch (ProtocolException e) {
			fail(e.getMessage());
			e.printStackTrace();		
		}
	}
	
	@Test
	public void testEncodeDecode() throws Exception {
        QoS qos = QoS.EXACTLY_ONCE;
        boolean retain = false;
        String topic = "testTopic";
        String payload = "foobar";
        PUBLISH input = new PUBLISH()
                        .qos(qos)
                        .retain(retain)
                        .topicName(Buffer.utf8(topic))
                        .payload(new Buffer(payload.getBytes()));
        PUBLISH output = new PUBLISH().decode(input.encode());
        Assert.assertEquals(qos, output.qos());
        Assert.assertEquals(retain, output.retain());
        Assert.assertEquals(topic, output.topicName().toString());
        Assert.assertEquals(payload, new String(output.payload().toByteArray()));
    }
	
	@Test
	public void deserializeAndSerializePacket() {
		PUBLISH p = new PUBLISH();
		p.payload(new Buffer("hello world".getBytes()));
		p.topicName(new UTF8Buffer("foo_topic"));
		
		try {
			byte header = p.encode().header();
			byte[] data = p.encode().buffers[0].data;
			MQTTPacket packet = new MQTTPacket(header, data);
			
			byte[] serialized = packet.serialize();
			
			assertEquals(header, serialized[0]);
			
			for (int i=0; i<data.length; i++) {
				assertEquals(data[i], serialized[1+i]);
			}
			
		} catch (ProtocolException e) {
			fail(e.getMessage());
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			fail(e.getMessage());
			e.printStackTrace();
		}

			
	}
}
