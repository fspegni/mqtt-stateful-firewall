#!/bin/sh

PORT1=1883
PORT2=9997
TOPIC1=/canale
TOPIC2=/canale
MSG="messaggio importante"

### OPERATIONS STARTS ###

# 1. start mosquitto service (must be installed)
sudo service mosquitto start

# 2. start interceptor (socat)
#echo "Starting socat..."
( echo "Starting socat..." ; socat -d -d -d -v TCP4-LISTEN:${PORT2} TCP4:localhost:${PORT1} 2>&1 | grcat red.conf & ) || { echo "Cannot continue: error starting socat" ; exit ; }
sleep 5

# 3. start subscriber
( echo "Starting mosquitto_sub ..." ; mosquitto_sub -t ${TOPIC1} -h localhost -p ${PORT2} 2>&1 | grcat green.conf & ) || { echo "Cannot continue: error starting mosquitto_sub" ; exit ; }
sleep 5

# 4. start publisher
echo "Starting mosquitto_pub ..." ; mosquitto_pub -t ${TOPIC2} -m "${MSG}"  2>&1 | grcat blue.conf 
sleep 1

# 5. cleanup
echo "Press enter to kill all socat and mosquitto_sub or Ctrl+C to skip this ... " && read FOO && killall socat mosquitto_sub
